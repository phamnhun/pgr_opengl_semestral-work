//----------------------------------------------------------------------------------------
/**
 * \file       spline.cpp
 * \author     Hung Pham
 * \date       2021
 * \brief      Utility functions and stuff tasks concerning animation curves.
*/
//----------------------------------------------------------------------------------------

#include "spline.h"


bool isVectorNull(const glm::vec3 &vect) {

  return !vect.x && !vect.y && !vect.z;
}


glm::mat4 alignObject(const glm::vec3 &position, const glm::vec3 &front, const glm::vec3 &up) {

  glm::vec3 z = -glm::normalize(front);

  if (isVectorNull(z))
    z = glm::vec3(0.0, 0.0, 1.0);

  glm::vec3 x = glm::normalize(glm::cross(up, z));

  if (isVectorNull(x))
    x =  glm::vec3(1.0, 0.0, 0.0);

  glm::vec3 y = glm::cross(z, x);
  glm::mat4 matrix = glm::mat4(
      x.x,        x.y,        x.z,        0.0,
      y.x,        y.y,        y.z,        0.0,
      z.x,        z.y,        z.z,        0.0,
      position.x, position.y, position.z, 1.0
  );

  return matrix;
}

/// Number of control points of the animation curve.
const size_t  curveSize = 6;


/// Control points of the animation curve.
glm::vec3 curveData[] = {
  glm::vec3(0.00, 0.0,  0.0),

  glm::vec3(-0.33,  0.35, 0.0),
  glm::vec3(-0.66,  0.35, 0.0),
  glm::vec3(-1.00,  0.0, 0.0),
  glm::vec3(-0.66, -0.35, 0.0),
  glm::vec3(-0.33, -0.35, 0.0),
};




glm::vec3 evaluateCurveSegment(
    const glm::vec3& P0,
    const glm::vec3& P1,
    const glm::vec3& P2,
    const glm::vec3& P3,
    const float t
) {
    glm::vec3 result(0.0, 0.0, 0.0);

    float t2 = t * t;
    float t3 = t2 * t;
    result =
          P0 * (-1 * t3 + 2 * t2 - 1 * t + 0 * 1)
        + P1 * ( 3 * t3 - 5 * t2 + 0 * t + 2 * 1)
        + P2 * (-3 * t3 + 4 * t2 + 1 * t + 0 * 1)
        + P3 * ( 1 * t3 - 1 * t2 + 0 * t + 0 * 1);

    result *= 0.5f;

    return result;
}

glm::vec3 evaluateCurveSegment_1stDerivative(
    const glm::vec3& P0,
    const glm::vec3& P1,
    const glm::vec3& P2,
    const glm::vec3& P3,
    const float t
) {
    glm::vec3 result(1.0, 0.0, 0.0);

    float t2 = 2 * t;
    float t3 = 3 * t * t;
    result =
          P0 * (-1 * t3 + 2 * t2 - 1 * 1)
        + P1 * ( 3 * t3 - 5 * t2 + 0 * 1)
        + P2 * (-3 * t3 + 4 * t2 + 1 * 1)
        + P3 * ( 1 * t3 - 1 * t2 + 0 * 1);

    result *= 0.5f;

    return result;
}

glm::vec3 evaluateClosedCurve(
    const glm::vec3 points[],
    const size_t    count,
    const float     t
) {
    glm::vec3 result(0.0, 0.0, 0.0);

    int i = (int)t;
    result = evaluateCurveSegment(
        points[(i - 1 + count) % count],
        points[ i              % count],
        points[(i + 1)         % count],
        points[(i + 2)         % count],
        t - i
    );

    return result;
}


glm::vec3 evaluateClosedCurve_1stDerivative(
    const glm::vec3 points[],
    const size_t    count,
    const float     t
) {
    glm::vec3 result(1.0, 0.0, 0.0);

    int i = (int)t;
    result = evaluateCurveSegment_1stDerivative(
        points[(i - 1 + count) % count],
        points[i % count],
        points[(i + 1) % count],
        points[(i + 2) % count],
        t - i
    );

    return result;
}
