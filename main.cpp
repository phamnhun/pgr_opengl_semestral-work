//----------------------------------------------------------------------------------------
/**
 * \file    main.cpp
 * \author  Hung Pham
 * \date    2021
 * \brief   Simple implementaion of Billiard bar game.
 */
 //----------------------------------------------------------------------------------------

// Semestral_work.cpp : This file contains the 'main' function. Program execution begins and ends there.
//



#include <iostream>
#include <time.h>
#include <list>
#include <glm/gtc/matrix_access.hpp>

#include "pgr.h"


#include "birds.h"
#include "render_stuff.h"
#include "spline.h"
#include "controller.h"
#include "gameObjects.h"
#include "gameState.h"
//#include "models.h"


using namespace std;



extern SCommonShaderProgram shaderProgram;
extern SkyboxFarPlaneShaderProgram skyboxFarPlaneShaderProgram;
extern bool useLighting;


GameObjects gameObjects;
GameState gameState;


void restartGame(void) {

    cleanUpObjects();
    gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds
    gameState.endingTime = 0.0f;
    gameState.beerDrunk = 0;

    // initialize player
    if (gameObjects.myPlayer == NULL)
        gameObjects.myPlayer = new PlayerObject;

    gameObjects.myPlayer->position = glm::vec3(0.0f, 0.0f, 0.0f);
    gameObjects.myPlayer->viewAngle = 90.0f; // degrees
    gameObjects.myPlayer->direction = glm::vec3(
                                        cos(glm::radians(gameObjects.myPlayer->viewAngle)), 
                                        sin(glm::radians(gameObjects.myPlayer->viewAngle)), 
                                        0.0f);
    gameObjects.myPlayer->speed = 0.0f;
    gameObjects.myPlayer->size = PLAYER_SIZE;
    gameObjects.myPlayer->destroyed = false;
    gameObjects.myPlayer->startTime = gameState.elapsedTime;
    gameObjects.myPlayer->currentTime = gameObjects.myPlayer->startTime;
    
    // initialize ground
    if (gameObjects.ground == NULL) {
        StaticObject* newObject = createStaticObject(true, glm::vec3(0.0f, 0.0f, 0.8f), glm::vec3(5.0f, 5.0f, 5.0f), glm::vec3(0, 0, 0), 0.0f);
        gameObjects.ground = newObject;
    }

    // initialize bird
    if (gameObjects.bird == NULL) {
        AnimatedObject* newObject = createAnimatedObject(BIRD_SIZE, glm::vec3(-1.75f, 0.0f, 0.7f), 0.0f, BIRD_SPEED);
        gameObjects.bird = newObject;
    }

    // initialize jukebox
    if (gameObjects.jukebox == NULL) {
        StaticObject* newJukebox = createJukebox(true, glm::vec3(0.35f, 0.9f, -0.15f), 0.2f, 0.0f);
        gameObjects.jukebox = newJukebox;
    }
    
    // initialize barstand
    if (gameObjects.barStand == NULL) {
        StaticObject* newObject = createStaticObject(true, glm::vec3(1.3f, -0.25f, -0.2f), glm::vec3(0.2f, 0.7f, 0.25f), glm::vec3(0, 0, 0), 0.0f);
        gameObjects.barStand = newObject;
    }

    // initialize television
    // // initialize television
    if (gameObjects.tvFrame == NULL) {
        StaticObject* newObject = createStaticObject(true, glm::vec3(1.0f, 0.95f, 0.2f), glm::vec3(0.35f, 0.35f, 0.35f), glm::vec3(0, 0, 0), 180.0f);
        gameObjects.tvFrame = newObject;
    }
    // initialize television screen
    if (gameObjects.television == NULL) {
        StaticObject* newObject = createStaticObject(true, glm::vec3(1.0f, 0.95f, 0.2f), glm::vec3(0.15f, 0.15f, 0.15f), glm::vec3(0, 0, 0), 0.0f);
        gameObjects.television = newObject;
    }


    // initialize ceiling lamp 
    if (gameObjects.ceilingLamp == NULL) {
        StaticObject* newObject;
        //newObject = createStaticObject(true, glm::vec3(0.0f, 0.0f, 0.75f), glm::vec3(0.4f, 0.4f, 0.4f), glm::vec3(0.0f, 0.0f, 0.0f), 0.0f);
        newObject = createStaticObject(true, glm::vec3(0.0f, 0.0f, 0.45f), glm::vec3(0.15f, 0.15f, 0.15f), glm::vec3(0.0f, 0.0f, 0.0f), 0.0f);
        gameObjects.ceilingLamp = newObject; 
        
        glUseProgram(shaderProgram.program);
        glUniform3fv(
            shaderProgram.lampPositionLocation,
            1,
            glm::value_ptr(gameObjects.ceilingLamp->position)
        );
        glUseProgram(0);
        toggleLamp();
    }

    // initialize floor 
    if (gameObjects.floor == NULL) {
        StaticObject* newObject;
        newObject = createStaticObject(true, glm::vec3(0.35f, 0.0f, 0.1f), glm::vec3(1.75f, 1.0f, 1.0f), glm::vec3(90.0f, 0.0f, 0.0f), 180.0f);
        gameObjects.floor = newObject;
    }

    // initialize bar shelf
    if (gameObjects.barShelf == NULL) {
        StaticObject* newObject = createStaticObject(true, glm::vec3(1.625f, 0.4f, 0.05f), glm::vec3(0.5f, 0.3f, 0.2f), glm::vec3(0, 0, 0), 270.0f);
        gameObjects.barShelf = newObject;
    }


    // initialize billiard table
    if (gameObjects.billiardTable == NULL) {
        StaticObject* newObject = createStaticObject(true, glm::vec3(0.0f, -0.5f, -0.22f), glm::vec3(0.3f, 0.3f, 0.3f), glm::vec3(90.0f, 180.0f, 0.0f), 0.0f);
        gameObjects.billiardTable = newObject;
    }
    
    // initialize light switches
    if (gameObjects.lightSwitches.size() < 1) {
        StaticObject* newObject = createStaticObject(true, glm::vec3(-0.4f, -0.95f, 0.0f), glm::vec3(0.025f, 0.025f, 0.025f), glm::vec3(0.0f, 0.0f, 0.0f), 0.0f);
        gameObjects.lightSwitches.push_back(newObject);
    }

    // initialize door
    if (gameObjects.door == NULL) {
        StaticObject* newObject = createStaticObject(true, glm::vec3(-0.65f, -0.95f, -0.1f), glm::vec3(0.45f, 0.45f, 0.4f), glm::vec3(0.0f, 0.0f, 0.0f), 180.0f);
        gameObjects.door = newObject;
    }

    // initialize wooden stools
    if (gameObjects.woodenStools.size() < 1) {
        unsigned int woodenStoolsInRow = 4;
        for (unsigned int i = 0; i < woodenStoolsInRow; i++) {
            StaticObject* newObject = createStaticObject(true, glm::vec3(1.1f,  0.2f * i - 0.3f, -0.3f), glm::vec3(0.05f, 0.05f, 0.08f), glm::vec3(0.0, 0.0, 0.0), 0.0f);
            gameObjects.woodenStools.push_back(newObject);
        }
        for (unsigned int i = 1; i < woodenStoolsInRow; i++) {
            StaticObject* newObject = createStaticObject(true, glm::vec3(1.1f, -0.2f * i - 0.3f, -0.3f), glm::vec3(0.05f, 0.05f, 0.08f), glm::vec3(0.0, 0.0, 0.0), 0.0f);
            gameObjects.woodenStools.push_back(newObject);
        }
    }

    // initialize beer bottles
    if (gameObjects.beerBottles.size() < 1) {
        unsigned int beerBottlesInRow = 4;
        for (unsigned int i = 0; i < beerBottlesInRow; i++) {
            StaticObject* newObject = createStaticObject(true, glm::vec3(1.25f, 0.2f * i - 0.31f, 0.0f), glm::vec3(0.05f, 0.05f, 0.05f), glm::vec3(0.0, 0.0, 0.0), 0.0f);
            gameObjects.beerBottles.push_back(newObject);
        }
        for (unsigned int i = 1; i < beerBottlesInRow; i++) {
            StaticObject* newObject = createStaticObject(true, glm::vec3(1.25f, -0.2f * i - 0.31f, 0.0f), glm::vec3(0.05f, 0.05f, 0.05f), glm::vec3(0.0, 0.0, 0.0), 0.0f);
            gameObjects.beerBottles.push_back(newObject);
        }
    }
    
    if (gameState.freeCameraMode == true) {
        gameState.freeCameraMode = false;
        glutPassiveMotionFunc(NULL);
    }
    
    gameState.cameraElevationAngle = 0.0f;

    // reset key map
    for (int i = 0; i < KEYS_COUNT; i++)
        gameState.keyMap[i] = false;

    gameState.gameOver = false;
}

void drawWindowContents() {


    glm::vec3 angledDirection = gameObjects.myPlayer->direction;          // THIS WILL BE USED LATER FOR DIRECTING LIGHT
    // setup parallel projection
    float scaledWidth = SCENE_WIDTH;
    if (gameState.windowHeight != gameState.windowWidth) {
        scaledWidth = (float)((float)gameState.windowWidth / (float)gameState.windowHeight);
    }
    glm::mat4 orthoProjectionMatrix = glm::ortho(
        -scaledWidth      , scaledWidth,
        -SCENE_HEIGHT     , SCENE_HEIGHT,
        0.8f * SCENE_DEPTH, 10.0f * SCENE_DEPTH             // Do not block the vision by te roof
        //-10.0f * SCENE_DEPTH, 10.0f * SCENE_DEPTH
        );
    glm::vec3 cameraPosition = glm::vec3(0.0f, 0.0f, 1.0f);
    // static viewpoint - top view
    glm::mat4 orthoViewMatrix = glm::lookAt(
        glm::vec3(0.0f, 0.0f, 1.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 1.0f, 0.0f)
    );

    // setup camera & projection transform
    glm::mat4 viewMatrix = orthoViewMatrix;
    glm::mat4 projectionMatrix = orthoProjectionMatrix;
    
    
    // SWITCH CAMERAS
    if (gameObjects.myPlayer->position.x > 0.9f 
        && 
        gameObjects.myPlayer->position.x < 1.0f
        &&
        gameState.cameraPositionState == 0) 
        gameState.cameraPositionState = 1;
    if (gameObjects.myPlayer->position.x > 0.3f
        &&
        gameObjects.myPlayer->position.x < 0.4f
        &&
        gameState.cameraPositionState == 1)
        gameState.cameraPositionState = 0;


    glUseProgram(shaderProgram.program);
    glUniform1f(shaderProgram.timeLocation, gameState.elapsedTime);
    glUniform1i(shaderProgram.inFirstPersonLocation, 0);
    glUseProgram(0);

    if (gameState.cameraPositionState == 1 && gameState.freeCameraMode == false) {
        
        cameraPosition.x = cameraPosition.x + 0.75f;
        orthoViewMatrix = glm::lookAt(
            cameraPosition,
            glm::vec3(0.0f + 0.75f, 0.0f, 0.0f),
            glm::vec3(0.0f, 1.0f, 0.0f)
        );
        viewMatrix = orthoViewMatrix;
        projectionMatrix = orthoProjectionMatrix;
            
    }
    else if (gameState.freeCameraMode == true /*gameState.cameraPositionState == 2*/) {
        ///*glm::vec3 */cameraPosition = glm::vec3(0.0f, 0.0f, 0.0f);
        cameraPosition = gameObjects.myPlayer->position;
        glm::vec3 cameraCenter = cameraPosition + (gameObjects.myPlayer->direction) * glm::vec3(0.1f, 0.1f, 0.1f);

        glm::vec3 cameraUpVector = glm::vec3(0.0f, 0.0f, 1.0f);

        glm::vec3 cameraViewDirection = gameObjects.myPlayer->direction;
        
        glm::vec3 rotAxis = glm::cross(cameraViewDirection, cameraUpVector);

        glm::mat4 rotMatrix = glm::rotate(
            glm::mat4(1.0f),
            glm::radians(gameState.cameraElevationAngle),
            rotAxis
        );

            //glm::vec3(rotMatrix * glm::vec4(gameObjects.myPlayer->direction, 0.0f) * glm::vec4(0.1f, 0.1f, 0.1f, 0.1f));
        cameraUpVector = glm::vec3(rotMatrix * glm::vec4(cameraUpVector, 0.0f));
        cameraViewDirection = glm::vec3(rotMatrix * glm::vec4(cameraViewDirection, 0.0f));
        cameraCenter = cameraPosition + cameraViewDirection;

        angledDirection = cameraViewDirection;

        viewMatrix = glm::lookAt(
            cameraPosition,
            cameraCenter,
            cameraUpVector
        );

        projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.005f, 10.0f);

        glUseProgram(shaderProgram.program);
        glUniform1f(shaderProgram.timeLocation, gameState.elapsedTime);
        glUniform1i(shaderProgram.inFirstPersonLocation, 1);

        glUseProgram(0);
        CHECK_GL_ERROR();
    }

    glUseProgram(shaderProgram.program);
    glUniform1f(shaderProgram.timeLocation, gameState.elapsedTime);


    glUniform3fv(
        shaderProgram.reflectorPositionLocation,
        1,
        glm::value_ptr(gameObjects.myPlayer->position)
    );
    
    glUniform3fv(
        shaderProgram.reflectorDirectionLocation,
        1,
        glm::value_ptr(angledDirection)
    );

    glUniform3fv(
        shaderProgram.cameraPositionLocation,
        1,
        glm::value_ptr(gameObjects.myPlayer->position)
    );
    glUseProgram(0);
    CHECK_GL_ERROR();
    
    // draw player
    if (!gameState.freeCameraMode) {
        drawPlayer(gameObjects.myPlayer, viewMatrix, projectionMatrix);
        CHECK_GL_ERROR();
    }


    // draw floor
    drawFloor(gameObjects.floor, viewMatrix, projectionMatrix);
    //CHECK_GL_ERROR();
    
    // Enable stencil test
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    CHECK_GL_ERROR();
    glStencilFunc(GL_ALWAYS, BILLIARD_TABLE_ID, -1);
    // draw billiard table
    drawBilliardTable(gameObjects.billiardTable, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();


    // draw light switches
    for (GameObjectsList::iterator it = gameObjects.lightSwitches.begin(); it != gameObjects.lightSwitches.end(); ++it) {
        StaticObject* currentObject = (StaticObject*)(*it);
        glStencilFunc(GL_ALWAYS, LIGHT_SWITCH_ID, -1);
        drawLightSwitch(currentObject, viewMatrix, projectionMatrix);
        CHECK_GL_ERROR();
    }
    // draw door
    glStencilFunc(GL_ALWAYS, EXIT_DOORS_ID, -1);
    drawDoor(gameObjects.door, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();

    // draw beer bottles
    unsigned int beerID = BEER_1;
    for (GameObjectsList::iterator it = gameObjects.beerBottles.begin(); it != gameObjects.beerBottles.end(); ++it) {
        StaticObject* currentObject = (StaticObject*)(*it);
        glStencilFunc(GL_ALWAYS, beerID, -1);
        drawBottle(currentObject, viewMatrix, projectionMatrix);
        beerID++;
        CHECK_GL_ERROR();
    }


    glDisable(GL_STENCIL_TEST);
    
    // draw
    drawBarShelf(gameObjects.barShelf, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();


    // draw ceiling lamp
    drawCeilingLamp(gameObjects.ceilingLamp, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();

    // draw bird
    drawBird(gameObjects.bird, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();


    // draw bar stand
    drawBarstand(gameObjects.barStand, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();

    // draw television
    drawTVFrame(gameObjects.tvFrame, viewMatrix, projectionMatrix);
    drawTelevision(gameObjects.television, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();

    //drawMultiMeshObject(billiardTable, viewMatrix, projectionMatrix, billiardTableGeometry);

    
    // draw only one jukebox
    drawJukebox(gameObjects.jukebox, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();


    // draw wooden stools
    for (GameObjectsList::iterator it = gameObjects.woodenStools.begin(); it != gameObjects.woodenStools.end(); ++it) {
        StaticObject* woodenStool = (StaticObject*)(*it);
        drawWoodenStool(woodenStool, viewMatrix, projectionMatrix);
        CHECK_GL_ERROR();
    }        

    

    drawSkybox(viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();



    // draw ground
    drawGround(gameObjects.ground, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();


    if (gameState.gameOver == true) {
        if (gameObjects.leavingBanner != NULL) {
            drawBanner(gameObjects.leavingBanner, orthoViewMatrix, orthoProjectionMatrix);
        }
    }
}

// Called to update the display. You should call glutSwapBuffers after all of your
// rendering to display what you rendered.
void displayCallback() {
    GLbitfield mask = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
    mask |= GL_STENCIL_BUFFER_BIT;

    glClear(mask);

    drawWindowContents();

    glutSwapBuffers();
}

// Called whenever the window is resized. The new window size is given, in pixels.
// This is an opportunity to call glViewport or glScissor to keep up with the change in size.
void reshapeCallback(int newWidth, int newHeight) {

    gameState.windowWidth = newWidth;
    gameState.windowHeight = newHeight;

    glViewport(0, 0, (GLsizei)newWidth, (GLsizei)newHeight);
}

void updateObjects(float elapsedTime) {

    // update banner
    if (gameObjects.leavingBanner != NULL) {
        gameObjects.leavingBanner->currentTime = elapsedTime;
    }
    
    
    // update player
    float timeDelta = elapsedTime - gameObjects.myPlayer->currentTime;
    gameObjects.myPlayer->currentTime = elapsedTime;

    

    glm::vec3 checkFuturePos = gameObjects.myPlayer->position;
    checkFuturePos += gameObjects.myPlayer->direction * gameObjects.myPlayer->speed * timeDelta * gameObjects.myPlayer->runMultiplier;
    checkFuturePos += gameObjects.myPlayer->sideDirection * gameObjects.myPlayer->sideWaySpeed * timeDelta * gameObjects.myPlayer->runMultiplier;

    // CHECK PLAYERS COLLISION
    if (!checkAllCollisionObjects(checkFuturePos)) {
        gameObjects.myPlayer->position = checkFuturePos;
    }


    // CLAMPING PLAYER MOVEMENT IN THE SCENE
    if (gameObjects.myPlayer->position.x < -0.9f) gameObjects.myPlayer->position.x = -0.9f;
    if (gameObjects.myPlayer->position.x > 1.65f) gameObjects.myPlayer->position.x = 1.65f;
    if (gameObjects.myPlayer->position.y < -0.9f) gameObjects.myPlayer->position.y = -0.9f;
    if (gameObjects.myPlayer->position.y > 0.9f) gameObjects.myPlayer->position.y = 0.9f;

    // update bird - make it fly
    gameObjects.bird->currentTime = elapsedTime;
    float curveParamT = gameObjects.bird->speed * (gameObjects.bird->currentTime - gameObjects.bird->startTime);
    gameObjects.bird->position = gameObjects.bird->initPosition + evaluateClosedCurve(curveData, curveSize, curveParamT);
    gameObjects.bird->direction = glm::normalize(evaluateClosedCurve_1stDerivative(curveData, curveSize, curveParamT));


    // update bird animation
    int timeMs = glutGet(GLUT_ELAPSED_TIME);						// time now [0.. infinity]
    int e = timeMs % ANIM_FRAME_TIME_MS;							// relative time in the frame [0..150ms]  <- animFrameTimeMs
    gameObjects.bird->frame = timeMs / ANIM_FRAME_TIME_MS;			// current frame index [0..10] ...        <- birds_data.nAnimFrames
    gameObjects.bird->nextFrame = gameObjects.bird->frame + 1;      // current frame + 1 modulo 11 [0..10]
    gameObjects.bird->frame %= birds_data.nAnimFrames;
    gameObjects.bird->nextFrame %= birds_data.nAnimFrames;
    gameObjects.bird->t = float(e) / float(ANIM_FRAME_TIME_MS);	    // relative time in the frame [0..1]      <- e % 150ms



    GameObjectsList::iterator it;

    // update television
    if (gameObjects.television != NULL) {
        if (gameObjects.television->destroyed == true) {
            delete gameObjects.television;
        }
        else {
            gameObjects.television->currentTime = elapsedTime;
        }
    }
}

// Callback responsible for the scene update
void timerCallback(int) {

    // update scene time
    gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

    // call appropriate actions according to the currently pressed keys in key map
    // (combinations of keys are supported but not used in this implementation)
    if (gameState.keyMap[KEY_RIGHT_ARROW] == true)
        if (gameState.freeCameraMode) walkSideways('r');
        else turnPlayerRight(PLAYER_VIEW_ANGLE_DELTA);
    else if (gameState.keyMap[KEY_LEFT_ARROW] == true)
        if (gameState.freeCameraMode) walkSideways('l');
        else turnPlayerLeft(PLAYER_VIEW_ANGLE_DELTA);
    else {
        stopStrafing();
    }

    if (gameState.keyMap[KEY_UP_ARROW] == true)
        walkForward();
    else if (gameState.keyMap[KEY_DOWN_ARROW] == true)
        walkBackward();
    else
        stopWalking();

    if (gameState.keyMap[KEY_SHIFT] == true)
        gameObjects.myPlayer->runMultiplier = 2.0f;
    else
        gameObjects.myPlayer->runMultiplier = 1.0f;


    // update objects in the scene
    updateObjects(gameState.elapsedTime);

    // space pressed
    if (gameState.keyMap[KEY_SPACE] == true) {
    }
    

    // game over? -> create banner with scrolling text "game over"
    if (gameState.gameOver == true) {
        for (int i = 0; i < KEYS_COUNT; i++) {
            gameState.keyMap[i] = false;
        }
        if (gameObjects.leavingBanner == NULL) {
            gameObjects.leavingBanner = createBanner();
        }
        if (gameState.elapsedTime - gameState.endingTime >= 20) {
    #ifndef __APPLE__
            glutLeaveMainLoop();
    #else
            exit(0);
    #endif
        }
    }

    glutTimerFunc(33, timerCallback, 0);

    glutPostRedisplay();
}

// Called whenever a key on the keyboard was pressed. The key is given by the "keyPressed"
// parameter, which is in ASCII. It's often a good idea to have the escape key (ASCII value 27)
// to call glutLeaveMainLoop() to exit the program.
void keyboardCallback(unsigned char keyPressed, int mouseX, int mouseY) {
    keyPressed = std::tolower(keyPressed);
    switch (keyPressed) {
    case 27: // escape
#ifndef __APPLE__
        glutLeaveMainLoop();
#else
        exit(0);
#endif
        break;
    case 'r': // restart game
        restartGame();
        break;
    case 't': // toggle fog
        if (gameState.gameOver != true)
            toggleFog();
        break;
    case 'c': // switch camera
        
        gameState.freeCameraMode = !gameState.freeCameraMode;
        if (gameState.freeCameraMode == true) {
            glutPassiveMotionFunc(passiveMouseMotionCallback);
            glutWarpPointer(gameState.windowWidth / 2, gameState.windowHeight / 2);
            glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);                          // Set cursor to crosshair
        }
        else {
            glutPassiveMotionFunc(NULL);
            glutSetCursor(GLUT_CURSOR_LEFT_ARROW);                              // Set default cursor
        }
        break;
    case 'f':   // press F to toggle fullscreen
        glutFullScreenToggle();
        break;
    case 'l':   // press L to toggle lamp
        if (gameState.gameOver != true)
            toggleLamp();
        break;
    case 'v':   // switch cameraPosition state
        gameState.cameraPositionState += 1;
        if (gameState.cameraPositionState == 2) gameState.cameraPositionState = 0;
        break;


    // WSAD controls
    case 'd':
        gameState.keyMap[KEY_RIGHT_ARROW] = true;
        break;
    case 'a':
        gameState.keyMap[KEY_LEFT_ARROW] = true;
        break;
    case 'w':
        gameState.keyMap[KEY_UP_ARROW] = true;
        break;
    case 's':
        gameState.keyMap[KEY_DOWN_ARROW] = true;
        break;
    default:
        ; // printf("Unrecognized key pressed\n");
    }
}

// Called whenever a key on the keyboard was released. The key is given by
// the "keyReleased" parameter, which is in ASCII. 
void keyboardUpCallback(unsigned char keyReleased, int mouseX, int mouseY) {
    keyReleased = std::tolower(keyReleased);
    switch (keyReleased) {
    case ' ':
        gameState.keyMap[KEY_SPACE] = false;
        break;

    // WSAD controls
    case 'd':
        gameState.keyMap[KEY_RIGHT_ARROW] = false;
        break;
    case 'a':
        gameState.keyMap[KEY_LEFT_ARROW] = false;
        break;
    case 'w':
        gameState.keyMap[KEY_UP_ARROW] = false;
        break;
    case 's':
        gameState.keyMap[KEY_DOWN_ARROW] = false;
        break;
    default:
        ; // printf("Unrecognized key released\n");
    }
}

// The special keyboard callback is triggered when keyboard function or directional
// keys are pressed.
void specialKeyboardCallback(int specKeyPressed, int mouseX, int mouseY) {

    if (gameState.gameOver == true)
        return;

    switch (specKeyPressed) {
    case GLUT_KEY_RIGHT:
        gameState.keyMap[KEY_RIGHT_ARROW] = true;
        break;
    case GLUT_KEY_LEFT:
        gameState.keyMap[KEY_LEFT_ARROW] = true;
        break;
    case GLUT_KEY_UP:
        gameState.keyMap[KEY_UP_ARROW] = true;
        break;
    case GLUT_KEY_DOWN:
        gameState.keyMap[KEY_DOWN_ARROW] = true;
        break;
    case GLUT_KEY_SHIFT_L:
        gameState.keyMap[KEY_SHIFT] = true;
        break;
    default:
        ; // printf("Unrecognized special key pressed\n");
    }
}

// The special keyboard callback is triggered when keyboard function or directional
// keys are released.
void specialKeyboardUpCallback(int specKeyReleased, int mouseX, int mouseY) {

    if (gameState.gameOver == true)
        return;

    switch (specKeyReleased) {
    case GLUT_KEY_RIGHT:
        gameState.keyMap[KEY_RIGHT_ARROW] = false;
        break;
    case GLUT_KEY_LEFT:
        gameState.keyMap[KEY_LEFT_ARROW] = false;
        break;
    case GLUT_KEY_UP:
        gameState.keyMap[KEY_UP_ARROW] = false;
        break;
    case GLUT_KEY_DOWN:
        gameState.keyMap[KEY_DOWN_ARROW] = false;
        break;
    case GLUT_KEY_SHIFT_L:
        gameState.keyMap[KEY_SHIFT] = false;
        break;
    default:
        ; // printf("Unrecognized special key released\n");
    }
}

// Called after the window and OpenGL are initialized. Called exactly once, before the main loop.
void initializeApplication() {

    // initialize random seed
    srand((unsigned int)time(NULL));

    // initialize OpenGL
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClearStencil(0);
    glEnable(GL_DEPTH_TEST);

    useLighting = true;

    // initialize shaders
    initializeShaderPrograms();
    // create geometry for all models used
    initializeModels();

    gameObjects.leavingBanner = NULL;
    gameObjects.myPlayer = NULL;
    gameState.fogOn = false;
    gameState.lampOn = false;

    restartGame();
}

void finalizeApplication(void) {

    cleanUpObjects();

    delete gameObjects.myPlayer;
    gameObjects.myPlayer = NULL;

    // delete buffers
    cleanupModels();

    // delete shaders
    cleanupShaderPrograms();
}

int main(int argc, char** argv) {

    // initialize windowing system
    glutInit(&argc, argv);

#ifndef __APPLE__
    glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#else
    glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#endif

    // initial window size
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutCreateWindow(WINDOW_TITLE);

    glutDisplayFunc(displayCallback);
    // register callback for change of window size
    glutReshapeFunc(reshapeCallback);
    // register callbacks for keyboard
    glutKeyboardFunc(keyboardCallback);
    glutKeyboardUpFunc(keyboardUpCallback);
    glutSpecialFunc(specialKeyboardCallback);     // key pressed
    glutSpecialUpFunc(specialKeyboardUpCallback); // key released

    glutMouseFunc(mouseCallback);

    glutTimerFunc(33, timerCallback, 0);

    // initialize PGR framework (GL, DevIl, etc.)
    if (!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
        pgr::dieWithError("pgr init failed, required OpenGL not supported?");

    initializeApplication();
    glutMainLoop();

#ifndef __APPLE__
    glutCloseFunc(finalizeApplication);
#else
    glutWMCloseFunc(finalizeApplication);
#endif


    return 0;
}



