//----------------------------------------------------------------------------------------
/**
 * \file    data.h
 * \author  Hung Pham
 * \date    2021
 * \brief   Basic defines and data structures for game state of billiard bar
 */
 //----------------------------------------------------------------------------------------

// keys used in the key map
enum { KEY_LEFT_ARROW, KEY_RIGHT_ARROW, KEY_UP_ARROW, KEY_DOWN_ARROW, KEY_SPACE, KEYS_COUNT, KEY_SHIFT };

typedef struct _GameState {

    int windowWidth;    // set by reshape callback
    int windowHeight;   // set by reshape callback

    bool freeCameraMode;        // false;
    float cameraElevationAngle; // in degrees = initially 0.0f

    int beerDrunk;

    bool gameOver;              // false;
    bool keyMap[KEYS_COUNT];    // false


    float endingTime;
    float elapsedTime;
    float missileLaunchTime;
    float ufoMissileLaunchTime;

    int cameraPositionState;

    bool fogOn;

    bool lampOn;

} GameState;
