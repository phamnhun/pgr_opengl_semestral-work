//----------------------------------------------------------------------------------------
/**
 * \file    controller.h
 * \author  Hung Pham
 * \date    2020-2021
 * \brief   Simple implementaion of Controls for Semestral work - billiard bar.
 */
 //----------------------------------------------------------------------------------------


void walkForward();
void walkBackward();
void walkSideways(char direction);
void stopWalking();
void stopStrafing();
void turnPlayerLeft(float deltaAngle);
void turnPlayerRight(float deltaAngle);
void toggleFog(void);
void toggleLamp(void);

void passiveMouseMotionCallback(int mouseX, int mouseY);
void mouseCallback(int buttonPressed, int buttonState, int mouseX, int mouseY);