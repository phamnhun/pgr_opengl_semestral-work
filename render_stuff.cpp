//----------------------------------------------------------------------------------------
/**
 * \file    render_stuff.cpp
 * \author  Hung Pham
 * \date    2021
 * \brief   Rendering stuff - drawing functions for models, etc..
 */
//----------------------------------------------------------------------------------------

#include <iostream>
#include <vector>
#include "pgr.h"


#include "render_stuff.h"
#include "data.h"
#include "spline.h"
#include "birds.h"

#include "gameState.h"

using namespace std;


MeshGeometry* playerGeometry = NULL;
MeshGeometry* jukeboxGeometry = NULL;
MeshGeometry* birdGeometry = NULL;
MeshGeometry* televisionGeometry = NULL;
MeshGeometry* tvframeGeometry = NULL;
MeshGeometry* beerbottleGeometry = NULL;

vector<MeshGeometry*> shelvesGeometry;

vector<MeshGeometry*> doorGeometry;
vector<MeshGeometry*> lightswitchGeometry;

vector<MeshGeometry*> billiardTableGeometry;
vector<MeshGeometry*> floorGeometry;
vector<MeshGeometry*> lampGeometry;
vector<MeshGeometry*> barstandGeometry;

MeshGeometry* bannerGeometry = NULL;

MeshGeometry* skyboxGeometry = NULL;
MeshGeometry* groundGeometry = NULL;


MeshGeometry* woodenStoolGeometry = NULL;



const char* PLAYER_MODEL_NAME = "data/models/simple_head/simple_head.obj";
const char* JUKEBOX_NAME = "data/models/jukebox/20344_Jukebox_bubbler_style_V2 Textured.obj";
const char* BILLIARD_TABLE_NAME = "data/models/billiard/PoolTable.obj";
const char* FLOOR_NAME = "data/models/wooden_floor/house_textured.obj";
const char* GROUND_NAME = "data/models/world_plane/ground.obj";
const char* LAMP_NAME = "data/models/chandelier/TorusKnotLight.obj";
const char* LIGHT_SWITCH_NAME = "data/models/light_switch/Light_Switch.obj";
const char* BAR_STAND_NAME = "data/models/bar_stand_obj/BarStand.obj";
const char* TELEVISION_NAME = "data/models/television/tv_frame.obj";
const char* TELEVISION_PROGRAM_NAME = "data/models/television/rickroll_spritesheet_square.png";
const char* DOOR_NAME = "data/models/door/Door 3.obj";
const char* BAR_SHELVES_NAME = "data/models/bar_shelves/bar_shelves.obj";
const char* BOTTLE_NAME = "data/models/beer_bottle/beer_bottle.obj";


const char* YOU_ARE_LEAVING_NAME = "data/gameOver.png";
const char* YOU_ARE_DRUNK_NAME = "data/drunk.png";


const char* SKYBOX_CUBE_TEXTURE_FILE_PREFIX = "data/worldTextures/skybox";


SCommonShaderProgram shaderProgram;
SkyboxFarPlaneShaderProgram skyboxFarPlaneShaderProgram;
BirdShaderProgram birdShaderProgram;
TelevisionShaderProgram televisionShaderProgram;
BannerShaderProgram bannerShaderProgram;


extern GameState gameState;

bool useLighting = false;

void setTransformUniforms(const glm::mat4& modelMatrix, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {

    glm::mat4 PVM = projectionMatrix * viewMatrix * modelMatrix;
    glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVM));

    glUniformMatrix4fv(shaderProgram.VmatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(shaderProgram.MmatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatrix));

    // just take 3x3 rotation part of the modelMatrix
    // we presume the last row contains 0,0,0,1
    const glm::mat4 modelRotationMatrix = glm::mat4(
        modelMatrix[0],
        modelMatrix[1],
        modelMatrix[2],
        glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
    );
    glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelRotationMatrix));

    //or an alternative single-line method: 
    //glm::mat4 normalMatrix = glm::transpose(glm::inverse(glm::mat4(glm::mat3(modelRotationMatrix))));

    glUniformMatrix4fv(shaderProgram.normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));  // correct matrix for non-rigid transform
}

void setMaterialUniforms(const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular, float shininess, GLuint texture) {

    glUniform3fv(shaderProgram.diffuseLocation,  1, glm::value_ptr(diffuse));  // 2nd parameter must be 1 - it declares number of vectors in the vector array
    glUniform3fv(shaderProgram.ambientLocation,  1, glm::value_ptr(ambient));
    glUniform3fv(shaderProgram.specularLocation, 1, glm::value_ptr(specular));
    glUniform1f(shaderProgram.shininessLocation,    shininess);

    if (texture != 0) {
        glUniform1i(shaderProgram.useTextureLocation, 1);  // do texture sampling
        glUniform1i(shaderProgram.texSamplerLocation, 0);  // texturing unit 0 -> samplerID   [for the GPU linker]
        glActiveTexture(GL_TEXTURE0 + 0);                  // texturing unit 0 -> to be bound [for OpenGL BindTexture]
        glBindTexture(GL_TEXTURE_2D, texture);
    }
    else {
        glUniform1i(shaderProgram.useTextureLocation, 0);  // do not sample the texture
    }
}


// Drawing leaving banner object
void drawBanner(BannerObject* banner, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glDisable(GL_DEPTH_TEST);

    glUseProgram(bannerShaderProgram.program);

    if (gameState.beerDrunk >= 5) {
        bannerGeometry->texture = pgr::createTexture(YOU_ARE_DRUNK_NAME);
    }

    glm::mat4 matrix = glm::translate(glm::mat4(1.0f), banner->position);
    matrix = glm::scale(matrix, glm::vec3(banner->size));
    matrix = glm::scale(matrix, glm::vec3(1.5f, 1.0f, 1.0f));

    glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
    glUniformMatrix4fv(bannerShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));        // model-view-projection
    glUniform1f(bannerShaderProgram.timeLocation, banner->currentTime - banner->startTime);
    glUniform1i(bannerShaderProgram.texSamplerLocation, 0);

    glBindTexture(GL_TEXTURE_2D, bannerGeometry->texture);
    glBindVertexArray(bannerGeometry->vertexArrayObject);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, bannerGeometry->numTriangles);

    CHECK_GL_ERROR();

    glBindVertexArray(0);
    glUseProgram(0);

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    return;
}


// Drawing bottle object
void drawBottle(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);
    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);

    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
        beerbottleGeometry->ambient,
        beerbottleGeometry->diffuse,
        beerbottleGeometry->specular,
        beerbottleGeometry->shininess,
        beerbottleGeometry->texture
    );
    // draw geometry
    glBindVertexArray(beerbottleGeometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, beerbottleGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}



// Drawing ground object
void drawGround(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);
    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);
    
    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
        groundGeometry->ambient,
        groundGeometry->diffuse,
        groundGeometry->specular,
        groundGeometry->shininess,
        groundGeometry->texture
    );
    // draw geometry
    glBindVertexArray(groundGeometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, groundGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}



    // Drawing jukebox object
void drawJukebox(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(currentObject->size, currentObject->size, currentObject->size));

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
        jukeboxGeometry->ambient,
        jukeboxGeometry->diffuse,
        jukeboxGeometry->specular,
        jukeboxGeometry->shininess,
        jukeboxGeometry->texture
    );
    // draw geometry
    glBindVertexArray(jukeboxGeometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, jukeboxGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

// Drawing television object
void drawTelevision(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(televisionShaderProgram.program);
    glBindVertexArray(televisionGeometry->vertexArrayObject);


    glm::mat4 matrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    matrix = glm::scale(matrix, currentObject->vecSize);

    glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
    glUniformMatrix4fv(televisionShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));  // model-view-projection
    glUniformMatrix4fv(televisionShaderProgram.VmatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));   // view
    glUniform1f(televisionShaderProgram.timeLocation, currentObject->currentTime - currentObject->startTime);
    
    
    televisionShaderProgram.texSamplerLocation = glGetUniformLocation(televisionShaderProgram.program, "texSampler");
    glUniform1i(televisionShaderProgram.texSamplerLocation, 0);
    
    //glUniform1f(televisionShaderProgram.frameDurationLocation, currentObject->frameDuration);               // unnecessary
    
    CHECK_GL_ERROR();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, televisionGeometry->texture);
    glDrawElements(GL_TRIANGLES, televisionGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    //glDisable(GL_BLEND);
    CHECK_GL_ERROR();
    return;
    
}


// Drawing TV frame
void drawTVFrame(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
        tvframeGeometry->ambient,
        tvframeGeometry->diffuse /*glm::vec3(0.0, 0.0, 0.0)*/,
        tvframeGeometry->specular,
        tvframeGeometry->shininess,
        tvframeGeometry->texture /*0*/
    );
    // draw geometry
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(tvframeGeometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, tvframeGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    glUseProgram(0);

    return;
}


// Drawing bar shelves object
void drawBarShelf(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    for (unsigned int i = 0; i < shelvesGeometry.size(); i++) {
        setMaterialUniforms(
            shelvesGeometry[i]->ambient,
            shelvesGeometry[i]->diffuse,
            shelvesGeometry[i]->specular,
            shelvesGeometry[i]->shininess,
            shelvesGeometry[i]->texture
        );
        // draw geometry
        glBindVertexArray(shelvesGeometry[i]->vertexArrayObject);
        glDrawElements(GL_TRIANGLES, shelvesGeometry[i]->numTriangles * 3, GL_UNSIGNED_INT, 0);
    }
    glBindVertexArray(0);
    glUseProgram(0);

    return;
}



// Drawing light switch object
void drawLightSwitch(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    for (unsigned int i = 0; i < lightswitchGeometry.size(); i++) {
        setMaterialUniforms(
            lightswitchGeometry[i]->ambient,
            lightswitchGeometry[i]->diffuse,
            lightswitchGeometry[i]->specular,
            lightswitchGeometry[i]->shininess,
            /*lightswitchGeometry[i]->texture*/ 0
        );
        // draw geometry
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(lightswitchGeometry[i]->vertexArrayObject);
        glDrawElements(GL_TRIANGLES, lightswitchGeometry[i]->numTriangles * 3, GL_UNSIGNED_INT, 0);
    }
    glBindVertexArray(0);
    glUseProgram(0);

    return;
}


// Drawing enter door object
void drawDoor(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    for (unsigned int i = 0; i < doorGeometry.size(); i++) {
        setMaterialUniforms(
            doorGeometry[i]->ambient,
            doorGeometry[i]->diffuse,
            doorGeometry[i]->specular,
            doorGeometry[i]->shininess,
            doorGeometry[i]->texture
        );
        // draw geometry
        glBindVertexArray(doorGeometry[i]->vertexArrayObject);
        glDrawElements(GL_TRIANGLES, doorGeometry[i]->numTriangles * 3, GL_UNSIGNED_INT, 0);
    }
    glBindVertexArray(0);
    glUseProgram(0);

    return;
}


// Drawing bar stand object
void drawBarstand(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    for (unsigned int i = 0; i < barstandGeometry.size(); i++) {
        setMaterialUniforms(
            barstandGeometry[i]->ambient,
            barstandGeometry[i]->diffuse,
            barstandGeometry[i]->specular,
            barstandGeometry[i]->shininess,
            barstandGeometry[i]->texture
        );
        // draw geometry
        glBindVertexArray(barstandGeometry[i]->vertexArrayObject);
        glDrawElements(GL_TRIANGLES, barstandGeometry[i]->numTriangles * 3, GL_UNSIGNED_INT, 0);
    }
    glBindVertexArray(0);
    glUseProgram(0);

    return;
}



    // Drawing billiard table object
void drawBilliardTable(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    for (unsigned int i = 0; i < billiardTableGeometry.size(); i++) {
        setMaterialUniforms(
            billiardTableGeometry[i]->ambient,
            billiardTableGeometry[i]->diffuse,
            billiardTableGeometry[i]->specular,
            billiardTableGeometry[i]->shininess,
            billiardTableGeometry[i]->texture
        );
        // draw geometry
        glBindVertexArray(billiardTableGeometry[i]->vertexArrayObject);
        glDrawElements(GL_TRIANGLES, billiardTableGeometry[i]->numTriangles * 3, GL_UNSIGNED_INT, 0);
    }
    

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

// Drawing lamp object
void drawCeilingLamp(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    for (unsigned int i = 0; i < lampGeometry.size(); i++) {
        setMaterialUniforms(
            lampGeometry[i]->ambient,
            lampGeometry[i]->diffuse,
            lampGeometry[i]->specular,
            lampGeometry[i]->shininess,
            lampGeometry[i]->texture
        );
        // draw geometry
        glBindVertexArray(lampGeometry[i]->vertexArrayObject);
        glDrawElements(GL_TRIANGLES, lampGeometry[i]->numTriangles * 3, GL_UNSIGNED_INT, 0);
    }


    glBindVertexArray(0);
    glUseProgram(0);

    return;
}


// Drawing floor object
void drawFloor(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);

    // prepare modeling transform matrix
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), currentObject->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(180.0f), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(currentObject->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, currentObject->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    for (unsigned int i = 0; i < floorGeometry.size(); i++) {
        setMaterialUniforms(
            floorGeometry[i]->ambient,
            floorGeometry[i]->diffuse,
            floorGeometry[i]->specular,
            floorGeometry[i]->shininess,
            floorGeometry[i]->texture
        );
        // draw geometry
        glBindVertexArray(floorGeometry[i]->vertexArrayObject);
        glDrawElements(GL_TRIANGLES, floorGeometry[i]->numTriangles * 3, GL_UNSIGNED_INT, 0);
    }


    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

void drawPlayer(PlayerObject* myPlayer, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {

  glUseProgram(shaderProgram.program);

  // prepare modeling transform matrix
  glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), myPlayer->position);
  modelMatrix = glm::rotate(modelMatrix, glm::radians(myPlayer->viewAngle + 90.0f), glm::vec3(0, 0, 1));
  modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(1, 0, 0));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(myPlayer->size, myPlayer->size, myPlayer->size));

  // send matrices to the vertex & fragment shader
  setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
  setMaterialUniforms(
      playerGeometry->ambient,
      playerGeometry->diffuse,
      playerGeometry->specular,
      playerGeometry->shininess,
      playerGeometry->texture
  );
  // draw geometry
  glBindVertexArray(playerGeometry->vertexArrayObject);
  glDrawElements(GL_TRIANGLES, playerGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

  glBindVertexArray(0);
  glUseProgram(0);

  return;
}


// Drawing bird object
void drawBird(AnimatedObject* bird, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {

    glUseProgram(birdShaderProgram.program);
    glm::mat4 modelMatrix;
    //modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    modelMatrix = alignObject(bird->position, -bird->direction, glm::vec3(0.0f, 0.0f, 1.0f));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(bird->size));

    glm::mat4 PVM = projectionMatrix * viewMatrix * modelMatrix;
    glUniformMatrix4fv(birdShaderProgram.PVMLocation, 1, GL_FALSE, glm::value_ptr(PVM));


    glUniform1f(birdShaderProgram.tLocation, bird->t);
    glUniform3fv(birdShaderProgram.colorLocation, 1, birds_data.color);
    
    // set fog on the model
    if (gameState.fogOn) {
        glUniform1i(birdShaderProgram.fogOnLocation, 1);
    }
    else {
        glUniform1i(birdShaderProgram.fogOnLocation, 0);
    }
    
    glBindVertexArray(birdGeometry->vertexArrayObject);
    glBindBuffer(GL_ARRAY_BUFFER, birdGeometry->vertexBufferObject);
    glVertexAttribPointer(birdShaderProgram.positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(bird->frame * birds_data.nVertices * 3 * sizeof(float)));
    //std::cout << birds_data.nVertices << std::endl;
    glVertexAttribPointer(birdShaderProgram.nextPositionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(bird->nextFrame * birds_data.nVertices * 3 * sizeof(float)));
    glDrawElements(GL_TRIANGLES, birdGeometry->numTriangles * 3, GL_UNSIGNED_SHORT, (void*)0);

    glBindVertexArray(0);
    glUseProgram(0);
    CHECK_GL_ERROR();
    return;
}


void drawWoodenStool(StaticObject* object, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {

    glUseProgram(shaderProgram.program);

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), object->position);
    modelMatrix = glm::rotate(modelMatrix, glm::radians(object->worldRotation.x), glm::vec3(1, 0, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(object->worldRotation.y), glm::vec3(0, 1, 0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(object->worldRotation.z), glm::vec3(0, 0, 1));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(object->viewAngle), glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, object->vecSize);

    // send matrices to the vertex & fragment shader
    setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
    // draw the wooden stool using glDrawArrays 
    glBindVertexArray(woodenStoolGeometry->vertexArrayObject);
    glDrawArrays(GL_TRIANGLES, 0, woodenStoolGeometry->numTriangles * 3);

    setMaterialUniforms(
        woodenStoolGeometry->ambient,
        woodenStoolGeometry->diffuse,
        woodenStoolGeometry->specular,
        woodenStoolGeometry->shininess,
        woodenStoolGeometry->texture
    );
    CHECK_GL_ERROR();

    // draw the six triangles of wooden stool using glDrawElements 
    glDrawElements(GL_TRIANGLES, woodenStoolGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

void drawSkybox(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {

    glUseProgram(skyboxFarPlaneShaderProgram.program);

    // compose transformations
    glm::mat4 matrix = projectionMatrix * viewMatrix;

    // create view rotation matrix by using view matrix with cleared translation
    glm::mat4 viewRotation = viewMatrix;
    viewRotation = glm::rotate(viewRotation, glm::radians(90.0f), glm::vec3(1, 0, 0));

    // vertex shader will translate screen space coordinates (NDC) using inverse PV matrix
    glm::mat4 inversePVmatrix = glm::inverse(projectionMatrix * viewRotation);

    glUniformMatrix4fv(skyboxFarPlaneShaderProgram.inversePVmatrixLocation, 1, GL_FALSE, glm::value_ptr(inversePVmatrix));
    glUniform1i(skyboxFarPlaneShaderProgram.skyboxSamplerLocation, 0);

    // draw "skybox" rendering 2 triangles covering the far plane
    glBindVertexArray(skyboxGeometry->vertexArrayObject);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxGeometry->texture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, skyboxGeometry->numTriangles + 2);

    glBindVertexArray(0);
    glUseProgram(0);
}


void cleanupShaderPrograms(void) {

  pgr::deleteProgramAndShaders(shaderProgram.program);
  
  pgr::deleteProgramAndShaders(skyboxFarPlaneShaderProgram.program);

  pgr::deleteProgramAndShaders(birdShaderProgram.program);
}


void initializeBannerShader(void) {
    std::vector<GLuint> shaderList;
    // push vertex shader and fragment shader
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "banner.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "banner.frag"));

    // Create the program with two shaders
    bannerShaderProgram.program = pgr::createProgram(shaderList);

    // get position and color attributes locations
    bannerShaderProgram.posLocation = glGetAttribLocation(bannerShaderProgram.program, "position");
    bannerShaderProgram.texCoordLocation = glGetAttribLocation(bannerShaderProgram.program, "texCoord");
    // get uniforms locations
    bannerShaderProgram.PVMmatrixLocation = glGetUniformLocation(bannerShaderProgram.program, "PVMmatrix");
    bannerShaderProgram.timeLocation = glGetUniformLocation(bannerShaderProgram.program, "time");
    bannerShaderProgram.texSamplerLocation = glGetUniformLocation(bannerShaderProgram.program, "texSampler");
    CHECK_GL_ERROR();
}

void initializeTelevisionShader(void) {
    std::vector<GLuint> shaderList;
    // push vertex shader and fragment shader
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "television.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "television.frag"));

    // create the program with two shaders
    televisionShaderProgram.program = pgr::createProgram(shaderList);

    // get position and texture coordinates attributes locations
    televisionShaderProgram.posLocation           = glGetAttribLocation(televisionShaderProgram.program, "position");
    televisionShaderProgram.texCoordLocation      = glGetAttribLocation(televisionShaderProgram.program, "texCoord");
    // get uniforms locations
    televisionShaderProgram.PVMmatrixLocation     = glGetUniformLocation(televisionShaderProgram.program, "PVMmatrix");
    televisionShaderProgram.VmatrixLocation       = glGetUniformLocation(televisionShaderProgram.program, "Vmatrix");
    televisionShaderProgram.timeLocation          = glGetUniformLocation(televisionShaderProgram.program, "time");
    televisionShaderProgram.texSamplerLocation    = glGetUniformLocation(televisionShaderProgram.program, "texSampler");
    televisionShaderProgram.frameDurationLocation = glGetUniformLocation(televisionShaderProgram.program, "frameDuration");
    CHECK_GL_ERROR();
}


void initializeSkyboxShader(void) {
    std::vector<GLuint> shaderList;
    // push vertex shader and fragment shader
    shaderList.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER,   skyboxFarPlaneVertexShaderSrc/*"skybox.vert"*/));
    shaderList.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, skyboxFarPlaneFragmentShaderSrc/*"skybox.frag"*/));

    // create the program with two shaders
    skyboxFarPlaneShaderProgram.program = pgr::createProgram(shaderList);

    // handles to vertex attributes locations
    skyboxFarPlaneShaderProgram.screenCoordLocation     = glGetAttribLocation(skyboxFarPlaneShaderProgram.program,  "screenCoord");
    // get uniforms locations
    skyboxFarPlaneShaderProgram.skyboxSamplerLocation   = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "skyboxSampler");
    skyboxFarPlaneShaderProgram.inversePVmatrixLocation = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "inversePVmatrix");

    // fog
    skyboxFarPlaneShaderProgram.fogOn        = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "fogOn");

    // daytime intensity
    skyboxFarPlaneShaderProgram.dayIntensity = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "dayIntensity");
}

void initializeBirdAnimationShader(void) {
    std::vector<GLuint> shaderList;
    // load and compile shader for raven animation

    // push vertex shader and fragment shader
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "birdAnimation.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "birdAnimation.frag"));

    // create the program with two shaders
    birdShaderProgram.program               = pgr::createProgram(shaderList);
    birdShaderProgram.positionLocation      = glGetAttribLocation(birdShaderProgram.program, "aPosition");
    birdShaderProgram.nextPositionLocation  = glGetAttribLocation(birdShaderProgram.program, "aNextPosition");

    birdShaderProgram.PVMLocation   = glGetUniformLocation(birdShaderProgram.program, "PVM");
    birdShaderProgram.tLocation     = glGetUniformLocation(birdShaderProgram.program, "t");
    birdShaderProgram.colorLocation = glGetUniformLocation(birdShaderProgram.program, "color");
    birdShaderProgram.fogOnLocation = glGetUniformLocation(birdShaderProgram.program, "fogOn");
}

void initializeLightShader(void) {

    std::vector<GLuint> shaderList;

    if (useLighting == true) {
        // load and compile shader for lighting (lights & materials)

        // push vertex shader and fragment shader
        shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER,    "lightingPerVertex.vert"));
        shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER,  "lightingPerVertex.frag"));

        // create the shader program with two shaders
        shaderProgram.program = pgr::createProgram(shaderList);

        // get vertex attributes locations, if the shader does not have this uniform -> return -1
        shaderProgram.posLocation       = glGetAttribLocation(shaderProgram.program,    "position");
        shaderProgram.normalLocation    = glGetAttribLocation(shaderProgram.program,    "normal");
        shaderProgram.texCoordLocation  = glGetAttribLocation(shaderProgram.program,    "texCoord");
        CHECK_GL_ERROR();
        // get uniforms locations
        shaderProgram.PVMmatrixLocation     = glGetUniformLocation(shaderProgram.program, "PVMmatrix");
        shaderProgram.VmatrixLocation       = glGetUniformLocation(shaderProgram.program, "Vmatrix");
        shaderProgram.MmatrixLocation       = glGetUniformLocation(shaderProgram.program, "Mmatrix");
        shaderProgram.normalMatrixLocation  = glGetUniformLocation(shaderProgram.program, "normalMatrix");
        shaderProgram.timeLocation          = glGetUniformLocation(shaderProgram.program, "time");
        // material
        shaderProgram.ambientLocation   = glGetUniformLocation(shaderProgram.program, "material.ambient");
        shaderProgram.diffuseLocation   = glGetUniformLocation(shaderProgram.program, "material.diffuse");
        shaderProgram.specularLocation  = glGetUniformLocation(shaderProgram.program, "material.specular");
        shaderProgram.shininessLocation = glGetUniformLocation(shaderProgram.program, "material.shininess");
        // texture
        shaderProgram.texSamplerLocation = glGetUniformLocation(shaderProgram.program, "texSampler");
        shaderProgram.useTextureLocation = glGetUniformLocation(shaderProgram.program, "material.useTexture");
        // reflector
        shaderProgram.reflectorPositionLocation  = glGetUniformLocation(shaderProgram.program, "reflectorPosition");
        shaderProgram.reflectorDirectionLocation = glGetUniformLocation(shaderProgram.program, "reflectorDirection");

        // fog
        shaderProgram.fogOn      = glGetUniformLocation(shaderProgram.program, "fogOn");

        shaderProgram.cameraPositionLocation = glGetUniformLocation(shaderProgram.program, "cameraPos");
        shaderProgram.inFirstPersonLocation  = glGetUniformLocation(shaderProgram.program, "inFirstPerson");

        shaderProgram.lampPositionLocation   = glGetUniformLocation(shaderProgram.program, "lampPosition");
        shaderProgram.lampOnLocation         = glGetUniformLocation(shaderProgram.program, "lampOn");
    }
    else {
        // load and compile simple shader (colors only, no lights at all)

        // push vertex shader and fragment shader
        shaderList.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, colorVertexShaderSrc));
        shaderList.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, colorFragmentShaderSrc));

        // create the program with two shaders (fragment and vertex)
        shaderProgram.program = pgr::createProgram(shaderList);
        // get position and color attributes locations
        shaderProgram.posLocation = glGetAttribLocation(shaderProgram.program, "position");
        shaderProgram.colorLocation = glGetAttribLocation(shaderProgram.program, "color");
        // get uniforms locations
        shaderProgram.PVMmatrixLocation = glGetUniformLocation(shaderProgram.program, "PVMmatrix");
    }

    CHECK_GL_ERROR();
}

void initializeShaderPrograms(void) {
    initializeLightShader();
    initializeSkyboxShader();
    initializeBirdAnimationShader();
    initializeTelevisionShader();
    initializeBannerShader();
}

/** Load mesh using assimp library
 *  Vertex, normals and texture coordinates data are stored without interleaving |VVVVV...|NNNNN...|tttt
 * \param fileName [in] file to open/load
 * \param shader [in] vao will connect loaded data to shader
 * \param geometry
 */
bool loadSingleMesh(const std::string& fileName, SCommonShaderProgram& shader, MeshGeometry** geometry) {
    Assimp::Importer importer;

    // Unitize object in size (scale the model to fit into (-1..1)^3)
    importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1);

    // Load asset from the file - you can play with various processing steps
    const aiScene* scn = importer.ReadFile(fileName.c_str(), 0
        | aiProcess_Triangulate             // Triangulate polygons (if any).
        | aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
        | aiProcess_GenSmoothNormals        // Calculate normals per vertex.
        | aiProcess_JoinIdenticalVertices);

    // abort if the loader fails
    if (scn == NULL) {
        std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
        *geometry = NULL;
        return false;
    }

    // some formats store whole scene (multiple meshes and materials, lights, cameras, ...) in one file, we cannot handle that in our simplified example
    if (scn->mNumMeshes != 1) {
        std::cerr << "this simplified loader can only process files with only one mesh" << std::endl;
        *geometry = NULL;
        return false;
    }

    // in this phase we know we have one mesh in our loaded scene, we can directly copy its data to OpenGL ...
    const aiMesh* mesh = scn->mMeshes[0];

    *geometry = new MeshGeometry;

    // vertex buffer object, store all vertex positions and normals
    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float) * mesh->mNumVertices, 0, GL_STATIC_DRAW); // allocate memory for vertices, normals, and texture coordinates
    // first store all vertices
    glBufferSubData(GL_ARRAY_BUFFER, 0, 3 * sizeof(float) * mesh->mNumVertices, mesh->mVertices);
    // then store all normals
    glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof(float) * mesh->mNumVertices, 3 * sizeof(float) * mesh->mNumVertices, mesh->mNormals);

    // just texture 0 for now
    float* textureCoords = new float[2 * mesh->mNumVertices];  // 2 floats per vertex
    float* currentTextureCoord = textureCoords;

    // copy texture coordinates
    aiVector3D vect;

    if (mesh->HasTextureCoords(0)) {
        // we use 2D textures with 2 coordinates and ignore the third coordinate
        for (unsigned int idx = 0; idx < mesh->mNumVertices; idx++) {
            vect = (mesh->mTextureCoords[0])[idx];
            *currentTextureCoord++ = vect.x;
            *currentTextureCoord++ = vect.y;
        }
    }

    // finally store all texture coordinates
    glBufferSubData(GL_ARRAY_BUFFER, 6 * sizeof(float) * mesh->mNumVertices, 2 * sizeof(float) * mesh->mNumVertices, textureCoords);

    // copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
    unsigned int* indices = new unsigned int[mesh->mNumFaces * 3];
    for (unsigned int f = 0; f < mesh->mNumFaces; ++f) {
        indices[f * 3 + 0] = mesh->mFaces[f].mIndices[0];
        indices[f * 3 + 1] = mesh->mFaces[f].mIndices[1];
        indices[f * 3 + 2] = mesh->mFaces[f].mIndices[2];
    }

    // copy our temporary index array to OpenGL and free the array
    glGenBuffers(1, &((*geometry)->elementBufferObject));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);

    delete[] indices;

    // copy the material info to MeshGeometry structure
    const aiMaterial* mat = scn->mMaterials[mesh->mMaterialIndex];
    aiColor4D color;
    aiString name;
    aiReturn retValue = AI_SUCCESS;

    // Get returns: aiReturn_SUCCESS 0 | aiReturn_FAILURE -1 | aiReturn_OUTOFMEMORY -3
    mat->Get(AI_MATKEY_NAME, name); // may be "" after the input mesh processing. Must be aiString type!

    if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color)) != AI_SUCCESS)
        color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);

    (*geometry)->diffuse = glm::vec3(color.r, color.g, color.b);

    if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color)) != AI_SUCCESS)
        color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
    (*geometry)->ambient = glm::vec3(color.r, color.g, color.b);

    if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color)) != AI_SUCCESS)
        color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
    (*geometry)->specular = glm::vec3(color.r, color.g, color.b);

    ai_real shininess, strength;
    unsigned int max;	// changed: to unsigned

    max = 1;
    if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS, &shininess, &max)) != AI_SUCCESS)
        shininess = 1.0f;
    max = 1;
    if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS_STRENGTH, &strength, &max)) != AI_SUCCESS)
        strength = 1.0f;
    (*geometry)->shininess = shininess * strength;

    (*geometry)->texture = 0;

    // load texture image
    if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
        // get texture name 
        aiString path; // filename

        aiReturn texFound = mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
        std::string textureName = path.data;

        size_t found = fileName.find_last_of("/\\");
        // insert correct texture file path 
        if (found != std::string::npos) { // not found
          //subMesh_p->textureName.insert(0, "/");
            textureName.insert(0, fileName.substr(0, found + 1));
        }

        std::cout << "Loading texture file: " << textureName << std::endl;
        (*geometry)->texture = pgr::createTexture(textureName);
    }
    CHECK_GL_ERROR();

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject); // bind our element array buffer (indices) to vao
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);

    glEnableVertexAttribArray(shader.posLocation);
    glVertexAttribPointer(shader.posLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
    CHECK_GL_ERROR();

    if (useLighting == true) {
        glEnableVertexAttribArray(shader.normalLocation);
        glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));
        CHECK_GL_ERROR();
    }
    else {
        glDisableVertexAttribArray(shader.colorLocation);
        // following line is problematic on AMD/ATI graphic cards
        // -> if you see black screen (no objects at all) than try to set color manually in vertex shader to see at least something
        glVertexAttrib3f(shader.colorLocation, color.r, color.g, color.b);
        CHECK_GL_ERROR();
    }
    CHECK_GL_ERROR();

    glEnableVertexAttribArray(shader.texCoordLocation);
    glVertexAttribPointer(shader.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
    CHECK_GL_ERROR();

    glBindVertexArray(0);

    (*geometry)->numTriangles = mesh->mNumFaces;

    return true;
}


/** Load mesh using assimp library
 *  Vertex, normals and texture coordinates data are stored without interleaving |VVVVV...|NNNNN...|tttt
 * \param fileName [in] file to open/load
 * \param shader [in] vao will connect loaded data to shader
 * \param geometry
 */
bool loadMultipleMesh(const string& fileName, SCommonShaderProgram& shader, vector<MeshGeometry*>& geometries) {
    Assimp::Importer importer;

    // Unitize object in size (scale the model to fit into (-1..1)^3)
    importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1);

    // Load asset from the file - you can play with various processing steps
    const aiScene* scn = importer.ReadFile(fileName.c_str(), 0
        | aiProcess_Triangulate             // Triangulate polygons (if any).
        | aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
        | aiProcess_GenSmoothNormals        // Calculate normals per vertex.
        | aiProcess_JoinIdenticalVertices);

    // abort if the loader fails
    if (scn == NULL) {
        std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
        for (unsigned int i = 0; i < geometries.size(); i++) {
            geometries[i] = NULL;
        }

        
        return false;
    }
    /*
    // some formats store whole scene (multiple meshes and materials, lights, cameras, ...) in one file, we cannot handle that in our simplified example
    if (scn->mNumMeshes != 1) {
        std::cerr << "this simplified loader can only process files with only one mesh" << std::endl;
        *geometry = NULL;
        return false;
    }
    */


    // in this phase we know we have one mesh in our loaded scene, we can directly copy its data to OpenGL ...
    for (unsigned int i = 0; i < scn->mNumMeshes; i++) {
        const aiMesh* mesh = scn->mMeshes[i];

        MeshGeometry* geometry = new MeshGeometry;

        // vertex buffer object, store all vertex positions and normals
        glGenBuffers(1, &((geometry)->vertexBufferObject));
        glBindBuffer(GL_ARRAY_BUFFER, (geometry)->vertexBufferObject);
        glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float) * mesh->mNumVertices, 0, GL_STATIC_DRAW); // allocate memory for vertices, normals, and texture coordinates
        // first store all vertices
        glBufferSubData(GL_ARRAY_BUFFER, 0, 3 * sizeof(float) * mesh->mNumVertices, mesh->mVertices);
        // then store all normals
        glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof(float) * mesh->mNumVertices, 3 * sizeof(float) * mesh->mNumVertices, mesh->mNormals);

        // just texture 0 for now
        float* textureCoords = new float[2 * mesh->mNumVertices];  // 2 floats per vertex
        float* currentTextureCoord = textureCoords;

        // copy texture coordinates
        aiVector3D vect;

        if (mesh->HasTextureCoords(0)) {
            // we use 2D textures with 2 coordinates and ignore the third coordinate
            for (unsigned int idx = 0; idx < mesh->mNumVertices; idx++) {
                vect = (mesh->mTextureCoords[0])[idx];
                *currentTextureCoord++ = vect.x;
                *currentTextureCoord++ = vect.y;
            }
        }

        // finally store all texture coordinates
        glBufferSubData(GL_ARRAY_BUFFER, 6 * sizeof(float) * mesh->mNumVertices, 2 * sizeof(float) * mesh->mNumVertices, textureCoords);

        // copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
        unsigned int* indices = new unsigned int[mesh->mNumFaces * 3];
        for (unsigned int f = 0; f < mesh->mNumFaces; ++f) {
            indices[f * 3 + 0] = mesh->mFaces[f].mIndices[0];
            indices[f * 3 + 1] = mesh->mFaces[f].mIndices[1];
            indices[f * 3 + 2] = mesh->mFaces[f].mIndices[2];
        }

        // copy our temporary index array to OpenGL and free the array
        glGenBuffers(1, &((geometry)->elementBufferObject));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (geometry)->elementBufferObject);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);

        delete[] indices;

        // copy the material info to MeshGeometry structure
        const aiMaterial* mat = scn->mMaterials[mesh->mMaterialIndex];
        aiColor4D color;
        aiString name;
        aiReturn retValue = AI_SUCCESS;

        // Get returns: aiReturn_SUCCESS 0 | aiReturn_FAILURE -1 | aiReturn_OUTOFMEMORY -3
        mat->Get(AI_MATKEY_NAME, name); // may be "" after the input mesh processing. Must be aiString type!

        if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color)) != AI_SUCCESS)
            color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);

        (geometry)->diffuse = glm::vec3(color.r, color.g, color.b);

        if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color)) != AI_SUCCESS)
            color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
        (geometry)->ambient = glm::vec3(color.r, color.g, color.b);

        if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color)) != AI_SUCCESS)
            color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
        (geometry)->specular = glm::vec3(color.r, color.g, color.b);

        ai_real shininess, strength;
        unsigned int max;	// changed: to unsigned

        max = 1;
        if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS, &shininess, &max)) != AI_SUCCESS)
            shininess = 1.0f;
        max = 1;
        if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS_STRENGTH, &strength, &max)) != AI_SUCCESS)
            strength = 1.0f;
        (geometry)->shininess = shininess * strength;

        (geometry)->texture = 0;

        // load texture image
        if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
            // get texture name 
            aiString path; // filename

            aiReturn texFound = mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
            std::string textureName = path.data;

            size_t found = fileName.find_last_of("/\\");
            // insert correct texture file path 
            if (found != std::string::npos) { // not found
              //subMesh_p->textureName.insert(0, "/");
                textureName.insert(0, fileName.substr(0, found + 1));
            }

            std::cout << "Loading texture file: " << textureName << std::endl;
            (geometry)->texture = pgr::createTexture(textureName);
        }
        CHECK_GL_ERROR();

        glGenVertexArrays(1, &((geometry)->vertexArrayObject));
        glBindVertexArray((geometry)->vertexArrayObject);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (geometry)->elementBufferObject); // bind our element array buffer (indices) to vao
        glBindBuffer(GL_ARRAY_BUFFER, (geometry)->vertexBufferObject);

        glEnableVertexAttribArray(shader.posLocation);
        glVertexAttribPointer(shader.posLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

        if (useLighting == true) {
            glEnableVertexAttribArray(shader.normalLocation);
            glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));
        }
        else {
            glDisableVertexAttribArray(shader.colorLocation);
            // following line is problematic on AMD/ATI graphic cards
            // -> if you see black screen (no objects at all) than try to set color manually in vertex shader to see at least something
            glVertexAttrib3f(shader.colorLocation, color.r, color.g, color.b);
        }

        glEnableVertexAttribArray(shader.texCoordLocation);
        glVertexAttribPointer(shader.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
        CHECK_GL_ERROR();

        glBindVertexArray(0);

        (geometry)->numTriangles = mesh->mNumFaces;

        geometries.push_back(geometry);

    }
    

    return true;
}


void initBirdGeometry(BirdShaderProgram& shader, MeshGeometry** geometry) {

    *geometry = new MeshGeometry;

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, birds_data.nAnimFrames * birds_data.nVertices * 3 * sizeof(float), birds_data.vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &((*geometry)->elementBufferObject));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, birds_data.nFaces * 3 * sizeof(unsigned short), birds_data.faces, GL_STATIC_DRAW);


    glEnableVertexAttribArray(shader.positionLocation);
    glVertexAttribPointer(shader.positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(2 * birds_data.nVertices * 3 * sizeof(float)));
    CHECK_GL_ERROR();
    glEnableVertexAttribArray(shader.nextPositionLocation);
    glVertexAttribPointer(shader.nextPositionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(birds_data.nVertices * 3 * sizeof(float)));

    (*geometry)->numTriangles = birds_data.nFaces;
    glBindVertexArray(0);
}



void initWoodenStoolGeometry(SCommonShaderProgram& shader, MeshGeometry** geometry) {

    *geometry = new MeshGeometry;

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, woodenStoolNAttribsPerVertex * sizeof(float) * woodenStoolNVertices, 
        woodenStoolVertices, 
        GL_STATIC_DRAW);

    glGenBuffers(1, &((*geometry)->elementBufferObject));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned int) * woodenStoolNTriangles, woodenStoolTriangles, GL_STATIC_DRAW);

    glEnableVertexAttribArray(shader.posLocation);
    // vertices of triangles - start at the beginning of the array
    glVertexAttribPointer(shader.posLocation,
        3, GL_FLOAT, GL_FALSE,
        woodenStoolNAttribsPerVertex * sizeof(float),
        0);

    if (useLighting == false) {
        glEnableVertexAttribArray(shader.colorLocation);
        // normal of vertex starts after the color (interlaced array)
        glVertexAttribPointer(shader.colorLocation,
            3, GL_FLOAT, GL_FALSE,
            woodenStoolNAttribsPerVertex * sizeof(float),
            0);
    }
    else {
        glEnableVertexAttribArray(shader.normalLocation);
        // normal of vertex starts after the color (interlaced array)
        glVertexAttribPointer(shader.normalLocation,
            3, GL_FLOAT, GL_FALSE,
            woodenStoolNAttribsPerVertex * sizeof(float),
            (void*)(3 * sizeof(float)));
    }

    (*geometry)->ambient = glm::vec3(0.0f, 1.0f, 1.0f);
    (*geometry)->diffuse = glm::vec3(1.0f, 0.75f, 0.5f);
    (*geometry)->specular = glm::vec3(1.0f, 0.75f, 0.75f);
    (*geometry)->shininess = 10.0f;
    (*geometry)->texture = 0;

    glBindVertexArray(0);

    (*geometry)->numTriangles = woodenStoolNTriangles;
}

void initTelevisionGeometry(GLuint shader, MeshGeometry** geometry) {

    *geometry = new MeshGeometry;

    (*geometry)->texture = pgr::createTexture(TELEVISION_PROGRAM_NAME);

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float) * televisionNVertices, televisionVertices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &((*geometry)->elementBufferObject));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned int) * televisionNTriangles, televisionTriangles, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    // vertices of triangles - start at the beginning of the array (interlaced array)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, televisionNAttribsPerVertex * sizeof(float), 0);
    

    glEnableVertexAttribArray(2);
    // texture coordinates are placed just after the position of each vertex (interlaced array)
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, televisionNAttribsPerVertex * sizeof(float), (void*)(6 * sizeof(float)));

    glBindVertexArray(0);

    (*geometry)->numTriangles = televisionNTriangles;
}

void initBannerGeometry(GLuint shader, MeshGeometry** geometry) {

    *geometry = new MeshGeometry;

    (*geometry)->texture = pgr::createTexture(YOU_ARE_LEAVING_NAME);
    glBindTexture(GL_TEXTURE_2D, (*geometry)->texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bannerVertexData), bannerVertexData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(bannerShaderProgram.posLocation);
    glEnableVertexAttribArray(bannerShaderProgram.texCoordLocation);
    // vertices of triangles - start at the beginning of the interlaced array
    glVertexAttribPointer(bannerShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
    // texture coordinates of each vertices are stored just after its position
    glVertexAttribPointer(bannerShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    glBindVertexArray(0);

    (*geometry)->numTriangles = bannerNumQuadVertices;
}


void initSkyboxGeometry(GLuint shader, MeshGeometry** geometry) {

    *geometry = new MeshGeometry;

    // 2D coordinates of 2 triangles covering the whole screen (NDC), draw using triangle strip
    static const float screenCoords[] = {
      -1.0f, -1.0f,
       1.0f, -1.0f,
      -1.0f,  1.0f,
       1.0f,  1.0f
    };

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    // buffer for far plane rendering
    glGenBuffers(1, &((*geometry)->vertexBufferObject)); \
        glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(screenCoords), screenCoords, GL_STATIC_DRAW);

    //glUseProgram(farplaneShaderProgram);

    glEnableVertexAttribArray(skyboxFarPlaneShaderProgram.screenCoordLocation);
    glVertexAttribPointer(skyboxFarPlaneShaderProgram.screenCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
    glUseProgram(0);
    CHECK_GL_ERROR();

    (*geometry)->numTriangles = 2;

    glActiveTexture(GL_TEXTURE0);

    glGenTextures(1, &((*geometry)->texture));
    glBindTexture(GL_TEXTURE_CUBE_MAP, (*geometry)->texture);

    const char* suffixes[] = { "posx", "negx", "posy", "negy", "posz", "negz" };
    GLuint targets[] = {
      GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
      GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
      GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    };

    for (int i = 0; i < 6; i++) {
        std::string texName = std::string(SKYBOX_CUBE_TEXTURE_FILE_PREFIX) + "_" + suffixes[i] + ".jpg";
        std::cout << "Loading cube map texture: " << texName << std::endl;
        if (!pgr::loadTexImage2D(texName, targets[i])) {
            pgr::dieWithError("Skybox cube map loading failed!");
        }
    }

    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    // unbind the texture (just in case someone will mess up with texture calls later)
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    CHECK_GL_ERROR();
}



/** Initialize vertex buffers and vertex arrays for all objects. 
 */
void initializeModels() {
    
    // load bottle model from external file
    if (loadSingleMesh(BOTTLE_NAME, shaderProgram, &beerbottleGeometry) != true) {
        std::cerr << "initializeModels(): Bottle model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // load ground model from external file
    if (loadSingleMesh(GROUND_NAME, shaderProgram, &groundGeometry) != true) {
        std::cerr << "initializeModels(): Ground model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // load head model from external file
    if(loadSingleMesh(PLAYER_MODEL_NAME, shaderProgram, &playerGeometry) != true) {
    std::cerr << "initializeModels(): Player model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // load jukebox from external file
    if (loadSingleMesh(JUKEBOX_NAME, shaderProgram, &jukeboxGeometry) != true) {
        std::cerr << "initializeModels(): Jukebox model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();
    
    
    // load television frame from external file
    if (loadSingleMesh(TELEVISION_NAME, shaderProgram, &tvframeGeometry) != true) {
        std::cerr << "initializeModels(): Television model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();
    
    // load doors from external file
    if (loadMultipleMesh(BAR_SHELVES_NAME, shaderProgram, shelvesGeometry) != true) {
        std::cerr << "initializeModels(): Bar shelves model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // load doors from external file
    if (loadMultipleMesh(DOOR_NAME, shaderProgram, doorGeometry) != true) {
        std::cerr << "initializeModels(): Door model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // load light switch from external file
    if (loadMultipleMesh(LIGHT_SWITCH_NAME, shaderProgram, lightswitchGeometry) != true) {
        std::cerr << "initializeModels(): Light switch model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // load bar stand from external file
    if (loadMultipleMesh(BAR_STAND_NAME, shaderProgram, barstandGeometry) != true) {
        std::cerr << "initializeModels(): Bar stand model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();
  
    // load billiard table from external file
    if (loadMultipleMesh(BILLIARD_TABLE_NAME, shaderProgram, billiardTableGeometry) != true) {
        std::cerr << "initializeModels(): Billiard table model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();
    
    // load house model from external file
    if (loadMultipleMesh(FLOOR_NAME, shaderProgram, floorGeometry) != true) {
        std::cerr << "initializeModels(): House model loading failed." << std::endl;      
    }
    CHECK_GL_ERROR();

    // load ceiling lamp model from external file
    if (loadMultipleMesh(LAMP_NAME, shaderProgram, lampGeometry) != true) {
        std::cerr << "initializeModels(): Ceiling lamp model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // fill MeshGeometry structures for hard coded objects
    initWoodenStoolGeometry(shaderProgram, &woodenStoolGeometry);
    initSkyboxGeometry(skyboxFarPlaneShaderProgram.program, &skyboxGeometry);
    initBirdGeometry(birdShaderProgram, &birdGeometry);

    initTelevisionGeometry(televisionShaderProgram.program, &televisionGeometry);

    initBannerGeometry(bannerShaderProgram.program, &bannerGeometry);

}

void cleanupGeometry(MeshGeometry *geometry) {

    glDeleteVertexArrays(1, &(geometry->vertexArrayObject));
    glDeleteBuffers(1, &(geometry->elementBufferObject));
    glDeleteBuffers(1, &(geometry->vertexBufferObject));

    if (geometry->texture != 0)
        glDeleteTextures(1, &(geometry->texture));

}

void cleanupGeometryVector(vector<MeshGeometry*> geometries) {
    for (unsigned int i = 0; i < geometries.size(); i++) {
        glDeleteVertexArrays(1, &(geometries[i]->vertexArrayObject));
        glDeleteBuffers(1, &(geometries[i]->elementBufferObject));
        glDeleteBuffers(1, &(geometries[i]->vertexBufferObject));

        if (geometries[i]->texture != 0)
            glDeleteTextures(1, &(geometries[i]->texture));
    }
}

void cleanupModels() {
  cleanupGeometry(playerGeometry);
  cleanupGeometry(groundGeometry);
  cleanupGeometry(jukeboxGeometry);
  cleanupGeometry(woodenStoolGeometry);
  cleanupGeometry(televisionGeometry);
  cleanupGeometry(tvframeGeometry);
  cleanupGeometry(beerbottleGeometry);


  cleanupGeometryVector(barstandGeometry);
  cleanupGeometryVector(billiardTableGeometry);
  cleanupGeometryVector(floorGeometry);
  cleanupGeometryVector(lampGeometry);
  cleanupGeometryVector(lightswitchGeometry);
  cleanupGeometryVector(doorGeometry);
  cleanupGeometryVector(shelvesGeometry);


  cleanupGeometry(birdGeometry);
  cleanupGeometry(skyboxGeometry);
  cleanupGeometry(bannerGeometry);
}
