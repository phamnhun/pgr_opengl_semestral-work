#version 140
	
  in vec3 aPosition;
  in vec3 aNextPosition;
  uniform mat4 PVM;
  uniform float t;

  void main() {

    vec3 pos = mix(aPosition, aNextPosition, t);

    gl_Position = PVM * vec4(pos, 1.0f);
  }