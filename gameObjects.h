//----------------------------------------------------------------------------------------
/**
 * \file    gameObjects.h
 * \author  Hung Pham
 * \date    2021
 * \brief   Header file for saved struct of GameObjects and their methods
 */
 //----------------------------------------------------------------------------------------


#include <list>
#include "render_stuff.h"

typedef std::list<void*> GameObjectsList;

typedef struct _GameObjects {

    PlayerObject* myPlayer; // NULL
    
    StaticObject* ground;
    
    StaticObject* billiardTable;
    StaticObject* jukebox;
    StaticObject* floor;
    StaticObject* door;

    StaticObject* ceilingLamp;
    StaticObject* barStand;
    StaticObject* television;
    StaticObject* tvFrame;
    StaticObject* barShelf;

    BannerObject* leavingBanner;
    BannerObject* drunkBanner;

    AnimatedObject* bird;
    
    GameObjectsList beerBottles;
    GameObjectsList lightSwitches;
    GameObjectsList woodenStools;
} GameObjects;


enum {
    BACKGROUND_ID,
    BILLIARD_TABLE_ID, 
    JUKEBOX_ID, 
    LIGHT_SWITCH_ID, 
    TELEVISION_ID, 
    EXIT_DOORS_ID, 
    BEER_1, 
    BEER_2, 
    BEER_3, 
    BEER_4, 
    BEER_5, 
    BEER_6, 
    BEER_7,
    STENCIL_OBJECTS
};

void cleanUpObjects(void);
AnimatedObject* createAnimatedObject(float size, glm::vec3 position, float viewAngle, float speed);
StaticObject* createJukebox(bool custom, glm::vec3 position, float size, float rotation);
StaticObject* createStaticObject(bool custom, glm::vec3 position, glm::vec3 size, glm::vec3 rotation, float Zrotation);
BannerObject* createBanner(void);


bool spheresIntersection(const glm::vec3& center1, float radius1, const glm::vec3& center2, float radius2);
bool pointToBoxCollision(glm::vec3 pointPos, glm::vec3 boxCenter, glm::vec3 boxOffset);
bool checkAllCollisionObjects(glm::vec3 checkFuturePos);
glm::vec3 collisionPosition(glm::vec3 pointPos, glm::vec3 direction, glm::vec3 boxCenter, glm::vec3 boxOffset);

