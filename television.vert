#version 330


layout(location=0) in vec3 position;           // vertex position in world space
layout(location=1) in vec3 normal;             // vertex normal
layout(location=2) in vec2 texCoord;           // incoming texture coordinates

uniform mat4 PVMmatrix;     // Projection * View * Model --> model to clip coordinates

smooth out vec2 texCoord_v; // outgoing vertex texture coordinates

void main() {

  // vertex position after the projection (gl_Position is predefined output variable)
  gl_Position = PVMmatrix * vec4(position, 1);   // outgoing vertex in clip coordinates

  // outputs entering the fragment shader
  texCoord_v = texCoord;
}
