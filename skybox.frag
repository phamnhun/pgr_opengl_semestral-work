// fragment shader uses interpolated 3D tex coords to sample cube map
#version 140

uniform samplerCube skyboxSampler;
in vec3 texCoord_v;
out vec4 color_f;

uniform bool fogOn;
uniform float dayIntensity;



float fogDistance() {
	float fog = 0.0f;
	//float density = 0.8f;
	float dist = 1.0f;
	float density = 3.0f;
	fog = clamp(exp(-pow(dist * density, 2)), 0, 1);

	return fog;
}

void main() {

    color_f = texture(skyboxSampler, texCoord_v);

	float fog = fogDistance();

    if (fogOn) {
        color_f = mix(vec4(0.6f, 0.6f, 0.6f, 1.0f), color_f, fog);
    }
	
}