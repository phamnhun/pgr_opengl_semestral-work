//----------------------------------------------------------------------------------------
/**
 * \file    asteroids.cpp
 * \author  Hung Pham
 * \date    2021
 * \brief   Simple implementaion of Controls for Semestral work - billiard bar.
 */
 //----------------------------------------------------------------------------------------
#include "pgr.h"
#include "data.h"
#include "gameObjects.h"
#include "gameState.h"


extern SCommonShaderProgram shaderProgram;
extern SkyboxFarPlaneShaderProgram skyboxFarPlaneShaderProgram;

extern GameObjects gameObjects;
extern GameState gameState;


// PLAYER MOVEMENT
//=========================================================================================
void walkForward() {
    gameObjects.myPlayer->speed = PLAYER_SPEED_MAX;
}

void walkBackward() {
    gameObjects.myPlayer->speed = -PLAYER_SPEED_MAX;
}

void walkSideways(char direction) {

    // GO LEFT
    float angle;
    if (direction == 'l') {
        angle = gameObjects.myPlayer->viewAngle + 90;
        gameObjects.myPlayer->rightDirection = glm::vec3(
            cos(glm::radians(angle)),
            sin(glm::radians(angle)),
            0.0f);
    }
    // GO RIGHT
    else if (direction == 'r') {
        angle = gameObjects.myPlayer->viewAngle + 270;
        gameObjects.myPlayer->leftDirection = glm::vec3(
            cos(glm::radians(angle)),
            sin(glm::radians(angle)),
            0.0f);
    }
    gameObjects.myPlayer->sideDirection = glm::vec3(
        cos(glm::radians(angle)),
        sin(glm::radians(angle)),
        0.0f);


    gameObjects.myPlayer->sideWaySpeed = PLAYER_SPEED_MAX;
}

void stopWalking() {
    gameObjects.myPlayer->speed = 0;
}

void stopStrafing() {
    gameObjects.myPlayer->sideWaySpeed = 0;
}

void turnPlayerLeft(float deltaAngle) {
    gameObjects.myPlayer->viewAngle += deltaAngle;
    if (gameObjects.myPlayer->viewAngle > 360.0f)
        gameObjects.myPlayer->viewAngle -= 360.f;

    float angle = glm::radians(gameObjects.myPlayer->viewAngle);

    gameObjects.myPlayer->direction = glm::vec3(
        cos(glm::radians(gameObjects.myPlayer->viewAngle)),
        sin(glm::radians(gameObjects.myPlayer->viewAngle)),
        0.0f
    );
}


void turnPlayerRight(float deltaAngle) {

    gameObjects.myPlayer->viewAngle -= deltaAngle;
    if (gameObjects.myPlayer->viewAngle < 360.0f)
        gameObjects.myPlayer->viewAngle += 360.f;

    float angle = glm::radians(gameObjects.myPlayer->viewAngle);

    gameObjects.myPlayer->direction = glm::vec3(
        cos(glm::radians(gameObjects.myPlayer->viewAngle)),
        sin(glm::radians(gameObjects.myPlayer->viewAngle)),
        0.0f
    );
}


//=========================================================================================



void toggleFog(void) {
    glUseProgram(shaderProgram.program);
    if (!gameState.fogOn) {
        glUniform1i(shaderProgram.fogOn, 1);
        gameState.fogOn = true;
    }
    else {
        glUniform1i(shaderProgram.fogOn, 0);
        gameState.fogOn = false;
    }
    glUseProgram(0);

    glUseProgram(skyboxFarPlaneShaderProgram.program);
    if (gameState.fogOn)
        glUniform1i(skyboxFarPlaneShaderProgram.fogOn, 1);
    else
        glUniform1i(skyboxFarPlaneShaderProgram.fogOn, 0);
    glUseProgram(0);
}

void toggleLamp(void) {
    glUseProgram(shaderProgram.program);
    if (!gameState.lampOn) {
        glUniform1i(shaderProgram.lampOnLocation, 1);
        gameState.lampOn = true;
    }
    else {
        glUniform1i(shaderProgram.lampOnLocation, 0);
        gameState.lampOn = false;
    }
    glUseProgram(0);
}



// Called when mouse is moving while no mouse buttons are pressed.
void passiveMouseMotionCallback(int mouseX, int mouseY) {

    if (mouseY != gameState.windowHeight / 2) {

        float cameraElevationAngleDelta = 0.5f * (mouseY - gameState.windowHeight / 2);
        cameraElevationAngleDelta *= -1;

        if (fabs(gameState.cameraElevationAngle + cameraElevationAngleDelta) < CAMERA_ELEVATION_MAX)
            gameState.cameraElevationAngle += cameraElevationAngleDelta;

        // set mouse pointer to the window center
        glutWarpPointer(gameState.windowWidth / 2, gameState.windowHeight / 2);

        glutPostRedisplay();
    }

    if (mouseX != gameState.windowWidth / 2) {

        float cameraTurnAngleDelta = 0.5f * (mouseX - gameState.windowWidth / 2);
        // Look Left
        if (cameraTurnAngleDelta < 0)
            turnPlayerLeft(-cameraTurnAngleDelta);
        // Look right
        else if (cameraTurnAngleDelta > 0)
            turnPlayerRight(cameraTurnAngleDelta);
        // set mouse pointer to the window center
        glutWarpPointer(gameState.windowWidth / 2, gameState.windowHeight / 2);

        glutPostRedisplay();
    }
}

void mouseCallback(int buttonPressed, int buttonState, int mouseX, int mouseY) {

    // do picking only on mouse down
    if ((buttonPressed == GLUT_LEFT_BUTTON) && (buttonState == GLUT_DOWN)) {

        // store value from the stencil buffer (byte)
        unsigned char objectID = 0;
        glReadPixels(
            mouseX, gameState.windowHeight - 1 - mouseY,
            1, 1,
            GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &objectID);                         // reading from stencil buffer

        // the buffer was initially cleared to zeros
        if (objectID == BACKGROUND_ID) {
            // background was clicked
            printf("Clicked on background\n");
            printf("beer 1 %d\n", BEER_1);
        }
        else {
            // object was clicked
            printf("Clicked on object with ID: %d\n", (int)objectID);

            if (objectID == BILLIARD_TABLE_ID) {
                printf("Clicked on billiard table\n");
            }
            else if (objectID == LIGHT_SWITCH_ID) {
                bool nearLightSwitch = false;
                for (GameObjectsList::iterator it = gameObjects.lightSwitches.begin(); it != gameObjects.lightSwitches.end(); ++it) {
                    StaticObject* lightSwitch = (StaticObject*)(*it);
                    if (spheresIntersection(gameObjects.myPlayer->position, gameObjects.myPlayer->size, lightSwitch->position, 0.2f))
                        nearLightSwitch = true;
                }
                if (nearLightSwitch)
                    toggleLamp();
            }
            else if (objectID == EXIT_DOORS_ID) {
                if (spheresIntersection(gameObjects.myPlayer->position, gameObjects.myPlayer->size, gameObjects.door->position, 0.3f)) {
                    gameState.gameOver = true;
                    if (gameState.endingTime == 0.0f) 
                        gameState.endingTime = gameState.elapsedTime;
                }
            }
            else if (objectID >= BEER_1 && objectID <= BEER_7) {
                printf("out of reach\n");
                for (GameObjectsList::iterator it = gameObjects.beerBottles.begin(); it != gameObjects.beerBottles.end(); /*++it*/) {
                    StaticObject* beerBottle = (StaticObject*)(*it);
                    if (spheresIntersection(gameObjects.myPlayer->position, gameObjects.myPlayer->size, beerBottle->position, 0.2f)) {
                        gameState.beerDrunk += 1;
                        printf("beer counter: %d\n", gameState.beerDrunk);
                        if (beerBottle != NULL) {
                            delete beerBottle;
                            beerBottle = NULL;
                            it = gameObjects.beerBottles.erase(it);
                        }
                        if (gameState.beerDrunk >= 5) {
                            gameState.gameOver = true;
                            if (gameState.endingTime == 0.0f)
                                gameState.endingTime = gameState.elapsedTime;
                        }
                    }
                    else {
                        ++it;
                    }
                }
            }
        }
    }
}