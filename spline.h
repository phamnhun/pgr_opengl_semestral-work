//----------------------------------------------------------------------------------------
/**
 * \file       spline.h
 * \author     Miroslav Miksik & Jaroslav Sloup & Petr Felkel
 * \date       2013
 * \brief      Utility functions and stuff tasks concerning animation curves.
*/
//----------------------------------------------------------------------------------------
#ifndef __SPLINE_H
#define __SPLINE_H

#include "pgr.h" // glm

bool isVectorNull(const glm::vec3 &vect);


glm::mat4 alignObject(const glm::vec3 &position, const glm::vec3 &front, const glm::vec3 &up);



extern glm::vec3 curveData[];


extern const size_t  curveSize;

template <typename T>
T cyclic_clamp(const T value, const T minBound, const T maxBound) {

    T amp = maxBound - minBound;
    T val = fmod(value - minBound, amp);

    if (val < T(0))
        val += amp;

    return val + minBound;
}

glm::vec3 evaluateCurveSegment(
    const glm::vec3& P0,
    const glm::vec3& P1,
    const glm::vec3& P2,
    const glm::vec3& P3,
    const float t
);

glm::vec3 evaluateCurveSegment_1stDerivative(
    const glm::vec3& P0,
    const glm::vec3& P1,
    const glm::vec3& P2,
    const glm::vec3& P3,
    const float t
);

glm::vec3 evaluateClosedCurve(
    const glm::vec3 points[],
    const size_t    count,
    const float     t
);

glm::vec3 evaluateClosedCurve_1stDerivative(
    const glm::vec3 points[],
    const size_t    count,
    const float     t
);


#endif // __SPLINE_H
