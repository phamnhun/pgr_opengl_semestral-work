#version 140
	
  uniform vec3 color;
  uniform bool fogOn;
  out vec4 fragmentColor;
  
  vec4 fogColor = vec4(0.8, 0.8, 0.8, 1.0);

  void main() {
    fragmentColor = vec4(color, 1.0f);

	if(fogOn){
		fragmentColor = mix(fragmentColor, fogColor, 0.8);
	}
  }
	