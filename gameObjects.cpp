//----------------------------------------------------------------------------------------
/**
 * \file    gameObjects.cpp
 * \author  Hung Pham
 * \date    2021
 * \brief   Implementation of methods to handle game objects.
 */
 //----------------------------------------------------------------------------------------


#include "pgr.h"
#include "gameObjects.h"
#include "gameState.h"

extern GameObjects gameObjects;
extern GameState gameState;

void cleanUpObjects(void) {
    // delete leaving banner
    if (gameObjects.leavingBanner != NULL) {
        delete gameObjects.leavingBanner;
        gameObjects.leavingBanner = NULL;
    }
    
    // delete bar shelf
    if (gameObjects.barShelf != NULL) {
        delete gameObjects.barShelf;
        gameObjects.barShelf = NULL;
    }
    
    // delete door
    if (gameObjects.door!= NULL) {
        delete gameObjects.door;
        gameObjects.door = NULL;
    }

    // delete ground
    if (gameObjects.ground != NULL) {
        delete gameObjects.ground;
        gameObjects.ground = NULL;
    }

    // delete house
    if (gameObjects.floor != NULL) {
        delete gameObjects.floor;
        gameObjects.floor = NULL;
    }

    // delete bird
    if (gameObjects.bird != NULL) {
        delete gameObjects.bird;
        gameObjects.bird = NULL;
    }

    // delete jukebox
    if (!gameObjects.jukebox != NULL) {
        delete gameObjects.jukebox;
        gameObjects.jukebox = NULL;
    }

    // delete bar stand
    if (!gameObjects.barStand != NULL) {
        delete gameObjects.barStand;
        gameObjects.barStand = NULL;
    }

    // delete billiard table
    if (gameObjects.billiardTable != NULL) {
        delete gameObjects.billiardTable;
        gameObjects.billiardTable = NULL;
    }

    // delete television
    if (gameObjects.television != NULL) {
        delete gameObjects.television;
        gameObjects.television = NULL;
    }
    if (gameObjects.tvFrame != NULL) {
        delete gameObjects.tvFrame;
        gameObjects.tvFrame = NULL;
    }

    // delete all beer bottles
    while (!gameObjects.beerBottles.empty()) {
        delete gameObjects.beerBottles.back();
        gameObjects.beerBottles.pop_back();
    }

    // delete all light switches
    while (!gameObjects.lightSwitches.empty()) {
        delete gameObjects.lightSwitches.back();
        gameObjects.lightSwitches.pop_back();
    }

    // delete all wooden stools
    while (!gameObjects.woodenStools.empty()) {
        delete gameObjects.woodenStools.back();
        gameObjects.woodenStools.pop_back();
    }
}


AnimatedObject* createAnimatedObject(float size, glm::vec3 position, float viewAngle, float speed) {
    AnimatedObject* newObject = new AnimatedObject;

    newObject->size = size;
    newObject->speed = speed;
    newObject->position = position;
    newObject->initPosition = position;
    newObject->direction = glm::vec3(sin(viewAngle), 0, cos(viewAngle));
    newObject->viewAngle = viewAngle;
    newObject->startTime = gameState.elapsedTime;
    newObject->currentTime = newObject->startTime;
    newObject->frame = 0;

    return newObject;
}


StaticObject* createJukebox(bool custom, glm::vec3 position, float size, float rotation) {
    StaticObject* jukebox = new StaticObject;

    jukebox->destroyed = false;
    jukebox->startTime = gameState.elapsedTime;
    jukebox->currentTime = jukebox->startTime;

    if (custom == true) {
        jukebox->size = size;
        jukebox->initPosition = position;
        jukebox->viewAngle = rotation;
    }
    else {
        jukebox->viewAngle = 0.0f;
        jukebox->size = 1.0f;
        jukebox->initPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    }

    jukebox->position = jukebox->initPosition;
    jukebox->speed = 0.0f;

    jukebox->direction = glm::vec3(sin(rotation), 0, cos(rotation));
    return jukebox;
}

StaticObject* createStaticObject(bool custom, glm::vec3 position, glm::vec3 size, glm::vec3 rotation, float Zrotation) {
    StaticObject* newObject = new StaticObject;

    newObject->destroyed = false;

    if (custom == true) {
        newObject->worldRotation = rotation;
        newObject->viewAngle = Zrotation;
        newObject->vecSize = size;
        newObject->initPosition = position;
    }
    else {
        newObject->viewAngle = 0.0f;
        newObject->vecSize = glm::vec3(1.0f, 1.0f, 1.0f);
        newObject->initPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    }

    newObject->position = newObject->initPosition;
    newObject->speed = 0;
    newObject->startTime = gameState.elapsedTime;
    newObject->currentTime = newObject->startTime;

    // generate randomly in range -1.0f ... 1.0f
    newObject->direction = glm::vec3(
        (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
        (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
        0.0f
    );
    newObject->direction = glm::normalize(newObject->direction);

    return newObject;
}

BannerObject* createBanner(void) {
    BannerObject* newBanner = new BannerObject;

    newBanner->size = BANNER_SIZE;
    newBanner->position = glm::vec3(0.0f, 0.0f, 0.0f);
    newBanner->direction = glm::vec3(0.0f, 1.0f, 0.0f);
    newBanner->speed = 0.0f;
    newBanner->size = 1.0f;

    newBanner->destroyed = false;

    newBanner->startTime = gameState.elapsedTime;
    newBanner->currentTime = newBanner->startTime;

    return newBanner;
}



bool spheresIntersection(const glm::vec3& center1, float radius1, const glm::vec3& center2, float radius2) {
    float dx = center1.x - center2.x;
    float dy = center1.y - center2.y;
    float dz = center1.z - center2.z;
    float dist = dx * dx + dy * dy + dz * dz;

    if (dist < (radius1 * radius1) + (radius2 * radius2)) {
        return true;
    }
    
    return false;
}


bool pointToBoxCollision(glm::vec3 pointPos, glm::vec3 boxCenter, glm::vec3 boxOffset) {
    return  (pointPos.x > boxCenter.x - boxOffset.x && pointPos.x < boxCenter.x + boxOffset.x)
             &&
            (pointPos.y > boxCenter.y - boxOffset.y && pointPos.y < boxCenter.y + boxOffset.y);
}

bool checkAllCollisionObjects(glm::vec3 checkFuturePos) {
    bool collision = false;
    if (pointToBoxCollision(checkFuturePos, gameObjects.jukebox->position, glm::vec3(0.175f, 0.1f, 0.0f)))
        collision = true;
    if (pointToBoxCollision(checkFuturePos, gameObjects.billiardTable->position, glm::vec3(0.2f, 0.325f, 0.0f)))
        collision = true;
    if (pointToBoxCollision(checkFuturePos, gameObjects.barStand->position, glm::vec3(0.15f, 0.7f, 0.0f)))
        collision = true;
    if (pointToBoxCollision(checkFuturePos, gameObjects.barShelf->position, glm::vec3(0.1f, 0.455f, 0.0f)))
        collision = true;
    return collision;
}


glm::vec3 collisionPosition(glm::vec3 pointPos, glm::vec3 direction, glm::vec3 boxCenter, glm::vec3 boxOffset) {
    glm::vec3 collisionPos;
    collisionPos = pointPos;
    
    glm::vec3 normDir = glm::normalize(direction);
    normDir.z = 0.0f;
    float offsetX;
    float offsetY;

    if (pointPos.x < boxCenter.x) {
        offsetX = abs(boxCenter.x - boxOffset.x - normDir.x);
    }

    else if (pointPos.x > boxCenter.x) {
        offsetX = abs(boxCenter.x + boxOffset.x - normDir.x);
    }
    
    if (pointPos.y < boxCenter.y) {
        offsetY = abs(boxCenter.y - boxOffset.y - normDir.y);
    }

    else if (pointPos.y > boxCenter.y) {
        offsetY = abs(boxCenter.y + boxOffset.y - normDir.y);
    }

    normDir = -normDir;
    normDir.x = normDir.x * offsetX;
    normDir.y = normDir.y * offsetY;

    collisionPos += normDir;
    return collisionPos;
}