//----------------------------------------------------------------------------------------
/**
 * \file    render_stuff.h
 * \author  Hung Pham
 * \date    2021
 * \brief   Rendering stuff - drawing functions for models, etc.
 */
//----------------------------------------------------------------------------------------

#ifndef __RENDER_STUFF_H
#define __RENDER_STUFF_H

#include <vector>
#include "data.h"


using namespace std;

// defines geometry of object in the scene (space ship, ufo, asteroid, etc.)
// geometry is shared among all instances of the same object type
typedef struct MeshGeometry {
	GLuint        vertexBufferObject;   // identifier for the vertex buffer object
	GLuint        elementBufferObject;  // identifier for the element buffer object
	GLuint        vertexArrayObject;    // identifier for the vertex array object
	unsigned int  numTriangles;         // number of triangles in the mesh
	// material
	glm::vec3     ambient;
	glm::vec3     diffuse;
	glm::vec3     specular;
	float         shininess;
	GLuint        texture;

} MeshGeometry;

// parameters of individual objects in the scene (e.g. position, size, speed, etc.)
typedef struct _Object {
  glm::vec3 position;
  glm::vec3 direction;
  glm::vec3 vecSize;

  float     speed;
  float     size;

  glm::vec3 worldRotation;	// in degrees
  float viewAngle;			// in degrees
  
  bool destroyed;

  float startTime;
  float currentTime;

} Object;

typedef struct _PlayerObject : public Object {

  float sideWaySpeed;
  float runMultiplier = 1.0f;
  glm::vec3 sideDirection;
  glm::vec3 rightDirection;
  glm::vec3 leftDirection;

} PlayerObject;

typedef struct _StaticObject : public Object {

	//float viewAngle; // in degrees
	glm::vec3 initPosition;

	int textureFrames;
	float frameDuration;

} StaticObject;

typedef struct _SplineObject : public Object {

	glm::vec3 initPosition;

} SplineObject;

typedef struct _AnimatedObject : public SplineObject {

	glm::vec3 color;	// raven color
	float t;			// interpolating time variable
	int frame;			// number of actual frame
	int nextFrame;		// number of next frame

} AnimatedObject;

typedef struct _BannerObject : public Object {
} BannerObject;

typedef struct _commonShaderProgram {
	// identifier for the shader program
	GLuint program;          // = 0;
	// vertex attributes locations
	GLint posLocation;       // = -1;
	GLint colorLocation;     // = -1;
	GLint normalLocation;    // = -1;
	GLint texCoordLocation;  // = -1;
	// uniforms locations
	GLint PVMmatrixLocation;    // = -1;
	GLint VmatrixLocation;      // = -1;  view/camera matrix
	GLint MmatrixLocation;      // = -1;  modeling matrix
	GLint normalMatrixLocation; // = -1;  inverse transposed Mmatrix

	GLint timeLocation;         // = -1; elapsed time in seconds

	// material 
	GLint diffuseLocation;    // = -1;
	GLint ambientLocation;    // = -1;
	GLint specularLocation;   // = -1;
	GLint shininessLocation;  // = -1;
	// texture
	GLint useTextureLocation; // = -1; 
	GLint texSamplerLocation; // = -1;
	// reflector related uniforms
	GLint reflectorPositionLocation;  // = -1; 
	GLint reflectorDirectionLocation; // = -1;

	// fog
	GLboolean fogOn;

	GLint cameraPositionLocation;
	GLboolean inFirstPersonLocation;

	GLint lampPositionLocation;
	GLboolean lampOnLocation;
} SCommonShaderProgram;


typedef struct _SkyboxFarPlaneShaderProgram {
	// identifier for the shader program
	GLuint program;                 // = 0;
	// vertex attributes locations
	GLint screenCoordLocation;      // = -1;
	// uniforms locations
	GLint inversePVmatrixLocation; // = -1;
	GLint skyboxSamplerLocation;    // = -1;

	GLfloat dayIntensity;
	GLint fogOn;
} SkyboxFarPlaneShaderProgram;


typedef struct _BirdShaderProgram {
	GLuint program;

	GLint positionLocation;
	GLint nextPositionLocation;
	GLint PVMLocation;
	GLint tLocation;
	GLint colorLocation;
	GLint fogOnLocation;
} BirdShaderProgram;

typedef struct _TelevisionShaderProgram {
	// identifier for the shader program
	GLuint program;              // = 0;
	// vertex attributes locations
	GLint posLocation;           // = -1;
	GLint texCoordLocation;      // = -1;
	// uniforms locations
	GLint PVMmatrixLocation;     // = -1;
	GLint VmatrixLocation;       // = -1;
	GLint timeLocation;          // = -1;
	GLint texSamplerLocation;    // = -1;
	GLint frameDurationLocation; // = -1;
} TelevisionShaderProgram;

typedef struct _BannerShaderProgram {
	// identifier for the shader program
	GLuint program;           // = 0;
	// vertex attributes locations
	GLint posLocation;        // = -1;
	GLint texCoordLocation;   // = -1;
	// uniforms locations
	GLint PVMmatrixLocation;  // = -1;
	GLint timeLocation;       // = -1;
	GLint texSamplerLocation; // = -1;
} BannerShaderProgram;

void drawGround(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);


void drawPlayer(PlayerObject* myPlayer, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);


void drawBanner(BannerObject* banner, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawDrunkBanner(BannerObject* banner, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);

void drawJukebox(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawBilliardTable(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawCeilingLamp(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawLightSwitch(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawDoor(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);

void drawBird(AnimatedObject* bird, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);

void drawBarstand(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawBarShelf(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawBottle(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);

void drawTVFrame(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawTelevision(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);


void drawFloor(StaticObject* currentObject, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);

void drawWoodenStool(StaticObject* object, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawSkybox(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);


void initializeShaderPrograms();
void cleanupShaderPrograms();

void initializeModels();
void cleanupModels();

#endif // __RENDER_STUFF_H


